import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://moldparts.com/quote'

test('Quoteform should work', async ({ page }) => {
    await page.goto(url);

    const formFields = [
        { label: 'Name', selector: '#edit-field-name-0-value', value: process.env.TESTER_FULLNAME},
        { label: 'Email', selector: '#edit-field-email-0-value', value: process.env.TESTER_EMAIL },
        { label: 'Phone number', selector: '#edit-field-phone-0-value', value: process.env.TESTER_PHONE },
        { label: 'Subject', selector: '#edit-field-subject-0-value', value: 'QA Automation Test' },
        { label: 'Description', selector: '#edit-field-description-0-value', value: process.env.TESTER_MESSAGE }
    ];

    // Check empty inputs.
    for (const { _label, selector } of formFields) {
        await page.waitForSelector(selector, { state: 'visible' });
        const locator = page.locator(selector);
        await expect(locator).toBeEmpty({ timeout: 10000 });
    }

    // Fill fields.
    for (const { selector, value } of formFields) {
        await page.fill(selector, value);
    }

    // Verify that the fields have been filled in.
    for (const { selector, value } of formFields) {
        const locator = page.locator(selector);
        await expect(locator).toHaveValue(value, { timeout: 10000 });
    }

    await page.getByText('Send message').click();

    await expect(page).toHaveURL('https://moldparts.com/');
});
