import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://prophysiotherapy.com/contact.php'

test.describe('Prophysiotherapy Contact Us', { tag: '@prophysiotherapy' }, () => {
    test('Contact Us form should work', async ({ page }) => {
        await page.goto(url);

        const formFields = [
            { label: 'Name', selector: '#username', value: process.env.TESTER_FULLNAME},
            { label: 'Email Address', selector: '#email', value: process.env.TESTER_EMAIL },
            { label: 'Phone number', selector: '#subject', value: process.env.TESTER_SUBJECT },
            { label: 'Message', selector: '#message', value: process.env.TESTER_MESSAGE }
        ];

        // Check empty inputs.
        for (const { _label, selector } of formFields) {
            await page.waitForSelector(selector, { state: 'visible' });
            const locator = page.locator(selector);
            await expect(locator).toBeEmpty({ timeout: 10000 });
        }

        // Fill fields.
        for (const { selector, value } of formFields) {
            await page.fill(selector, value);
        }

        // Verify that the fields have been filled in.
        for (const { selector, value } of formFields) {
            const locator = page.locator(selector);
            await expect(locator).toHaveValue(value, { timeout: 10000 });
        }

        await page.getByRole('button', { name: 'Send Email' }).click();
        await expect(page.getByRole('heading', { name: 'Congratulations!' })).toBeVisible();
    });
});
