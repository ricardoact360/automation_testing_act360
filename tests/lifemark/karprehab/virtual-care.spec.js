import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://karprehab.com/virtual-care/'

test.describe('Karprehab Vitual Care', { tag: '@karprehab' }, () => {
  test('Accordion should work', async ({ browser }) => {
      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });
      const page = await context.newPage();
      await page.goto(url);
      await page.getByRole('tab', { name: 'What kind of equipment do I' }).click();
      await expect(page.getByText('A laptop, smartphone (Android')).toBeVisible();
      await page.getByRole('tab', { name: 'Can I join a session from my' }).click();
      await expect(page.getByText('Either. A number of services')).toBeVisible();
      await page.getByRole('tab', { name: 'How do I make a Virtual Care' }).click();
      await expect(page.getByText('If you are interested in')).toBeVisible();
      await page.getByRole('tab', { name: 'Does Virtual Care cover' }).click();
      await expect(page.getByText('Some regulatory and licensing')).toBeVisible();
      await page.getByRole('tab', { name: 'How can I get ready for a' }).click();
      await expect(page.getByText('To ensure you are ready for')).toBeVisible();
      await page.getByRole('tab', { name: 'What can I expect for my' }).click();
      await expect(page.getByText('At the beginning of a virtual')).toBeVisible();
      await page.getByRole('tab', { name: 'Is it possible to perform a' }).click();
      await expect(page.getByText('Yes, virtual services are')).toBeVisible();
      await page.getByRole('tab', { name: 'Is it possible to perform a' }).click();
      await expect(page.getByRole('tab', { name: 'Is it possible to perform a' })).toBeVisible();
      await page.getByRole('tab', { name: 'How can I get ready for a' }).click();
      await expect(page.getByRole('tab', { name: 'How can I get ready for a' })).toBeVisible();
  });
});
