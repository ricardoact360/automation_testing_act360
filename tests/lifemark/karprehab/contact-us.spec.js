import { test, expect } from "@playwright/test";
import dotenv from "dotenv";

dotenv.config();

const url = 'https://karprehab.com/vancouver-rehabilitation-contact-information/'
const emailUrl = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${process.env.MAILNATOR_ID}`

// NOTE: This is working fine just need the Auth! <<<<<<<<<<<<<<<< Remove skip
// AND UNCOMMENT THE END!!!!!!
// test.use({ storageState: "./auth/auth-karprehab.json" })
test.describe('Karprehab Contact Us', { tag: '@karprehab' }, () => {
  test.skip('Contact Us form should work', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });
      const page = await context.newPage();
      await page.goto(url);

      const formFields = [
          { label: 'First name', selector: 'input[name="input_1"]', value: process.env.TESTER_FULLNAME},
          { label: 'Email Address', selector: 'input[name="input_3"]', value: process.env.TESTER_EMAIL },
          { label: 'Phone number', selector: 'input[name="input_4"]', value: "(231) 232-2334" }, // This phone format it's necessary
          { label: 'Message', selector: 'textarea[name="input_6"]', value: process.env.TESTER_MESSAGE }
      ];

      // Press checkbox
      await page.locator('input[name="input_5.2"]').check();
      await page.locator('input[name="input_5.4"]').check();
      await page.locator('input[name="input_5.8"]').check();

      // Fill fields.
      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      /* await page.getByText('Send').click(); */
      /* await page.waitForTimeout(10000) */
      /**/
      /* await expect(page.locator('text="Thanks for contacting us! We will get in touch with you shortly."')).toBeVisible(); */
      /**/
      /* await page.goto(emailUrl, { waitUntil: "domcontentloaded" }); //  Solution! */
      /* await page.waitForTimeout(10000) */
      /* await page.getByRole('cell', { name: 'Thank you for contacting Karp Rehab' }).first().click(); */
      /* await expect(page.frameLocator('iframe[name="html_msg_body"]').getByText('Thank you for contacting Karp Rehab! Below is a copy of your request for your records. We will contact you shortly.')).toBeVisible(); */
  });
});

