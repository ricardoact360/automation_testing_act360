import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://karprehab.com/referrals/'

// NOTE: This is working fine just need the Auth! <<<<<<<<<<<<<<<< Remove skip
// test.use({ storageState: "./auth/auth-karprehab.json" })
test.describe('Karprehab Contact Us', { tag: '@karprehab' }, () => {
  test.skip('Referrals Us form should work', async ({ page }) => {
      await page.goto(url);

      const formFields = [
          { label: 'First name', selector: 'input[name="input_1"]', value: process.env.TESTER_FULLNAME},
          { label: 'Email Address', selector: 'input[name="input_3"]', value: process.env.TESTER_EMAIL },
          { label: 'Phone number', selector: 'input[name="input_4"]', value: "(231) 232-2334" }, // This phone format it's necessary
          { label: 'Message', selector: 'textarea[name="input_6"]', value: process.env.TESTER_MESSAGE }
      ];

      // Press checkbox
      await page.locator('input[name="input_5.2"]').check();

      // Fill fields.
      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      /* await page.getByText('Submit').click(); */
      /* await page.waitForTimeout(10000) */
      /**/
      /* await expect(page.locator('text="Thanks for contacting us! We will get in touch with you shortly."')).toBeVisible(); */
  });
});
