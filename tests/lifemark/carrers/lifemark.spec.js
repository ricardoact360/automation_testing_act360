import { test, expect } from "@playwright/test";
import { chromium } from "playwright";
import dotenv from "dotenv";
dotenv.config();

/* 
(async () => {
  const browser = await chromium.launch({ headless: false });
  const context = await browser.newContext({
      httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }, // UAT username and password here
      // viewport: { width, height }
  });

  const page = await context.newPage();
  await page.goto('https://www.lifemark.ca/current-opportunities');

  await page.waitForSelector('.pane-content ul li');


})();
*/

(async () => {
  const browser = await chromium.launch({ headless: true });
  const context = await browser.newContext({
    httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD },
  });

  const page = await context.newPage();
  await page.goto('https://www.lifemark.ca/current-opportunities');

  await page.waitForSelector('.pane-content ul li');

  const listItems = await page.$$('.pane-content ul li');
  const pageAnalysis = [];

  for (const item of listItems) {
    const linkElement = await item.$('.views-field-field-link a');
    if (!linkElement) continue;

    const href = await linkElement.getAttribute('href');
    const title = await linkElement.innerText();
    // console.log(`Checking link: ${href}`);

    const newPage = await context.newPage();
    await newPage.goto(href);
    await newPage.waitForLoadState('networkidle');

    const currentUrl = newPage.url();
    const urlParams = new URL(currentUrl).searchParams;
    const ccId = urlParams.get('ccId');

    if (ccId !== '19000101_000001') {
      pageAnalysis.push({
        title: title,
        url: currentUrl,
        ccId: ccId,
      });
      console.log(`Found different ccId: ${ccId}`);
    }
    await newPage.close();
  }

  console.log('PageAnalysis:', pageAnalysis);

  await browser.close();
})();
