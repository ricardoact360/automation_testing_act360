import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://www.lifemarkhealthgroup.ca/contact-us'

function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function getRandomFloat(min, max) {
  return (Math.random() * (max - min) + min).toFixed(2);
}

// npx playwright codegen https://www.lifemarkhealthgroup.ca/user/login --save-storage=auth-lifemarkhealthgroup.json
test.use({ storageState: "./auth/auth-lifemarkhealthgroup.json" })
test.describe('lifemarkhealthgroup Contact Us', { tag: '@lifemarkhealthgroup' }, () => {

  test('Contact Us Form should work', async ({ page }) => {
    await page.goto(url);

    // Simulate human-like page load and initial viewing
    await delay(getRandomFloat(2000, 5000));

    const formFields = [
      { label: 'Name', selector: '#edit-name', value: process.env.TESTER_FULLNAME },
      { label: 'Email Address', selector: '#edit-email', value: process.env.TESTER_EMAIL },
      { label: 'Phone number', selector: '#edit-telephone', value: process.env.TESTER_PHONE },
      { label: 'Company', selector: '#edit-subject', value: process.env.TESTER_COMPANY },
      { label: 'Message', selector: '#edit-message', value: process.env.TESTER_MESSAGE }
    ];

    // Simulate scrolling
    await page.evaluate(() => {
      window.scrollBy(0, 500);
    });
    await delay(getRandomFloat(1000, 2000));

    // How can we help you?
    await page.selectOption('#edit-how-can-i-help-you-', 'Medical Assessments');
    await delay(getRandomFloat(500, 1500));

    // Fill fields with human-like delays and typing speed
    for (const { selector, value } of formFields) {
      await page.focus(selector);
      await delay(getRandomFloat(300, 800));
      
      for (const char of value) {
        await page.type(selector, char, { delay: Math.floor(getRandomFloat(50, 150)) });
      }
      
      await delay(getRandomFloat(500, 1500));
    }

    // Simulate final review before submission
    await page.evaluate(() => {
      window.scrollBy(0, -200);
    });
    await delay(getRandomFloat(2000, 4000));

    await page.getByRole('button', { name: 'Submit>' }).click();
    
    // Wait for submission and check for confirmation
    await page.waitForTimeout(10000);
    await expect(page.locator('text="Thank you for the form submission!"')).toBeVisible();
  });
});
