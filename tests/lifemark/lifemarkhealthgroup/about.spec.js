import { test, expect } from "@playwright/test";

const url = 'https://www.lifemarkhealthgroup.ca/about-lifemark-health-group'

test.use({
  viewport: { height: 800, width: 360 }
});

test.describe('lifemarkhealthgroup About Us', { tag: '@lifemarkhealthgroup' }, () => {
  test('Accordion should work', async ({ page }) => {
    await page.goto(url);
    await page.locator('#block-organizationchart div').filter({ hasText: 'Community Rehabilitation' }).nth(4).click();
    await expect(page.getByText('Rehabilitation clinics In-')).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark.ca', exact: true })).toBeVisible();
    await expect(page.getByRole('link', { name: 'PT Health' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Universelle' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Pro Physio' })).toBeVisible();
    await page.locator('.subColumn > .divisionBox').first().click();
    await expect(page.locator('.subColumn > .accordionItem').first()).toBeVisible();
    await page.locator('div:nth-child(2) > .divisionBox').click();
    await expect(page.locator('div:nth-child(2) > .accordionItem')).toBeVisible();
    await page.locator('.subColumn > .divisionBox').first().click();
    await expect(page.locator('.subColumn > .accordionItem').first()).toBeVisible();
    await page.locator('div:nth-child(2) > .divisionBox').click();
    await expect(page.locator('div:nth-child(2) > .accordionItem')).toBeVisible();
  });
});

