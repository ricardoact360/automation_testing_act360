import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

test('Fill the form & Confirm Email - 15 refreshes', async ({ browser }) => {
  const context = await browser.newContext({
    httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
  });
  const page = await context.newPage();

  for (let i = 0; i < 15; i++) {
    console.log(`Attempt ${i + 1} of 15`);
    
    await page.goto('https://bookingqa.lifemark.ca/OnlineBooking/index?locationID=4459388&serviceId=10014&brand=1');
    
    // Wait for the page to load
    await page.waitForLoadState('networkidle');

    // Check if the main elements are visible
    await expect(page.getByRole('heading')).toContainText('Okay! Now tell us a bit more about your Physiotherapy needs at 11 Victoria Street...', { timeout: 10000 });
    await expect(page.locator('#step_2')).toContainText('Would you like to book a virtual appointment?*', { timeout: 10000 });
    await expect(page.locator('#non-virtual')).toContainText('Have you attended our clinic in the last 3 months for the service you are requesting and for the same area of concern?*', { timeout: 10000 });
    await expect(page.getByText('Reason for your visit:*')).toBeVisible({ timeout: 10000 });
    await expect(page.locator('#step3continue')).toContainText('Continue', { timeout: 10000 });

    console.log(`Page loaded successfully on attempt ${i + 1}`);

    // NOTE: Short delay between refreshes to avoid overwhelming the server
    if (i < 14) {
      await page.waitForTimeout(1000);
    }
  }
});
