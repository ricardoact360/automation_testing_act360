// NOTE: Tests based on https://act360web.atlassian.net/wiki/spaces/LIF/pages/2765225986/QA+Document
// NOTE: Run with "fullyParallel: false" on the playwright.config.js
// TEST: firefox_load & this file
import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://bookingqa.lifemark.ca/OnlineBooking/index/?locationID=73846&l_source=bookingqa.lifemark.ca&l_medium=referral&l_content=unknown&l_campaign=generic&brand=1&serviceId=10014&brand=1'
const emailUrl = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${process.env.MAILNATOR_ID}`;

function extractConfirmationLink(jsonText) {
  try {
    const data = JSON.parse(jsonText);
    console.log("data in extractConfirmationLink: ", data)
    const confirmLink = data.clickablelinks.find(item => 
      item.link.startsWith('https://bookingqa.lifemark.ca/confirm')
    );
    return confirmLink ? confirmLink.link : null;
  } catch (error) {
    console.log("Error in function extractConfirmationLink()")
    console.error('Error reading JSON:', error);
    return null;
  }
}

test.describe('Lifemark OLB', { tag: '@OLB' }, () => {

  test('Should trigger the events in GTM', async ({ browser }) => {
    test.setTimeout(120 * 1000);

    const context = await browser.newContext({
      httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });
    const page = await context.newPage();

    await page.goto('https://tagassistant.google.com/');
    await page.getByRole('button', { name: 'Add domain' }).click();
    await page.getByPlaceholder('e.g. https://www.google.com').click();
    await page.getByPlaceholder('e.g. https://www.google.com').fill(url);
    await page.waitForTimeout(10000) // Wait 
    const page1Promise = page.waitForEvent('popup');
    await page.getByLabel('Opens your site in a new').click();

    const page1 = await page1Promise;

    await page1.locator('#step_2 div').filter({ hasText: 'Would you like to book a' }).getByLabel('No').check();
    await page1.locator('#non-virtual').getByLabel('No').check();
    await page1.getByText('I\'m paying for myself').click();
    await page1.getByRole('link', { name: 'Select' }).click();
    await page1.locator('#reason-for-visit').click();
    await page1.locator('#reason-for-visit').fill(process.env.TESTER_MESSAGE);
    await page1.getByRole('link', { name: 'Continue' }).click();
    await page1.waitForSelector('.slot-row.therapist-row', { timeout: 80000 });

    await page1.waitForSelector('a.btn.btn-sm.btn-default.time-slot-button');
    /* await page1.click('a.btn.btn-sm.btn-default.time-slot-button'); */
    await page1.waitForTimeout(3000); // Wait! 
    // NOTE: Be aware! This is the "position" of the Appointment's hour
    await page1.locator('a.btn.btn-sm.btn-default.time-slot-button').nth(1).click(); 
    // await page1.locator('a.btn.btn-sm.btn-default.time-slot-button').click(); 

    await page1.getByLabel('First Name: *').fill(process.env.TESTER_NAME);
    await page1.getByLabel('Last Name: *').fill(process.env.TESTER_LASTNAME);
    await page1.getByLabel('Phone: *').fill(process.env.TESTER_PHONE);
    await page1.getByLabel('Email: *').fill(process.env.TESTER_EMAIL);

    await page1.waitForTimeout(5000); // Wait! 
    await page1.getByRole('link', { name: 'Request Appointment Now' }).click();
    await page1.waitForTimeout(5000); // Wait! 
    await expect(page1.getByText('Check your email and confirm')).toBeAttached();

    await page1.waitForTimeout(10000); // Wait! This is the "trick" to avoid red alert / weird message

    await page.getByRole('button', { name: 'Continue', exact: true }).click();

    // GTM tags:
    await expect(page.locator('container-picker-ng')).toContainText('G-T97HKMXPMG');
    await expect(page.locator('container-picker-ng')).toContainText('GTM-TLCKR4Q');
    await expect(page.locator('container-picker-ng')).toContainText('AW-849990860');

    await page.locator('div[role="button"][data-ng-repeat^="message in"]').filter({ hasText: 'Online_Booking_Step2' }).nth(0).click();
    await expect(page.getByText('Event: Online_Booking_Step2')).toBeVisible();
    await page.getByRole('button', { name: 'Online_Booking_Step3' }).click();
    await expect(page.getByText('Event: Online_Booking_Step3')).toBeVisible();
    await page.getByRole('button', { name: 'Online_Booking_Step4' }).click();
    await expect(page.getByText('Event: Online_Booking_Step4')).toBeVisible();
    /* await page.getByRole('button', { name: 'OnlineBooking_ConfirmationSent' }).click(); */
    await page.getByTitle('OnlineBooking_ConfirmationSent').click();
    await expect(page.locator('debugger-content-component')).toContainText('Event: OnlineBooking_ConfirmationSent');
  });

  test('Should attribution firing in email link', async ({ browser }) => {
    test.setTimeout(120 * 1000);
    const context = await browser.newContext({
        httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });
    const page = await context.newPage();
    await page.goto(emailUrl);
    await page.getByRole('cell', { name: 'ACTION REQUIRED - Confirm' }).first().click();
    await page.getByRole('tab', { name: 'JSON' }).click();
    await page.waitForTimeout(5000); // Wait! 

    const preContent = await page.evaluate(() => {
      const preElement = document.querySelector('#pills-json-content pre');
      return preElement ? preElement.textContent : null;
    });

    if (preContent) {
      console.log('HERE')
      const confirmationLink = extractConfirmationLink(preContent)
      console.log("-- Confirmation Link: ", confirmationLink)

      await page.goto('https://tagassistant.google.com/');
      await page.getByRole('button', { name: 'Add domain' }).click();
      await page.getByPlaceholder('e.g. https://www.google.com').click();
      await page.getByPlaceholder('e.g. https://www.google.com').fill(confirmationLink);
      const page2Promise = page.waitForEvent('popup');
      await page.getByLabel('Opens your site in a new').click();
      const page2 = await page2Promise;
      await page.waitForTimeout(7000); // Wait! 
      // await expect(page2.locator('#step_final')).toContainText('Thank-you, your appointment has been booked!');
      const confirmationMessage = page2.locator('#step_final h2');
      await expect(confirmationMessage).toBeVisible();
      await expect(confirmationMessage).toHaveText('Thank-you, your appointment has been booked!');
      await page.getByRole('button', { name: 'Continue', exact: true }).click();
      await expect(page.getByRole('button', { name: 'History Change' })).toBeVisible();
      await page.getByRole('button', { name: 'History Change' }).click();
      await page.getByText('API Call', { exact: true }).click();
      await expect(page.getByRole('button', { name: 'gtag("event", "page_view", {...})', exact: true })).toBeVisible();
      await expect(page.getByRole('button', { name: 'Online_Booking_Confirmed' })).toBeVisible();
      await page.getByRole('button', { name: 'Online_Booking_Confirmed' }).click();
      await expect(page.getByRole('button', { name: 'gtag("event", "Online_Booking_Confirmed", {...})', exact: true })).toBeVisible();

      await expect(page.getByRole('button', { name: 'conversion' })).toBeVisible();
      await page.getByRole('button', { name: 'conversion' }).click();
      await expect(page.getByRole('button', { name: 'gtag("event", "conversion", {...})', exact: true })).toBeVisible();

      await page2.goto('https://bookingqa.lifemark.ca/confirm?guid=e3a9586a-6c63-c0a4-4cc0-be91c50f788c&locale=en&utm_source=bookingqa.lifemark.ca&utm_medium=referral&utm_content=unknown&utm_campaign=generic&gtm_debug=1725990598690');
      await expect(page2.getByRole('heading', { name: 'It looks like you have' })).toBeVisible();

    } else {
      console.log("We did not find <pre>");
    }
  });

  test('Should check if Email has arrive & "confirm" in the URL should be lowercase', async ({ browser }) => {
    const context = await browser.newContext({
        httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });
    const page = await context.newPage();
    await page.goto(emailUrl);
    await page.getByRole('cell', { name: 'ACTION REQUIRED - Confirm' }).first().click();
    await page.getByRole('tab', { name: 'JSON' }).click();
    await page.waitForTimeout(5000); // Wait! 

    const preContent = await page.evaluate(() => {
      const preElement = document.querySelector('#pills-json-content pre');
      return preElement ? preElement.textContent : null;
    });

    if (preContent) {
      console.log('HEREEEEEE')
      const confirmationLink = extractConfirmationLink(preContent)
      console.log("confirmationLink: ", confirmationLink)

      function isConfirmLowercase(url) {
        const urlObject = new URL(url);
        const path = urlObject.pathname;
        const confirmIndex = path.toLowerCase().indexOf('confirm');

        if (confirmIndex === -1) {
          return false;
        }
        
        const confirmWord = path.substr(confirmIndex, 7);
        return confirmWord === confirmWord.toLowerCase();
      }

      const isConfirmLinkLowecase = isConfirmLowercase(confirmationLink);
      expect(isConfirmLinkLowecase).toBe(true, 'The "confirm" in the URL should be lowercase');

    } else {
      console.log("We did not find <pre>");
    }
  });

  // NOTE: Open email and check
  test('Should show appointment is already confirmed', async ({ browser }) => {
    const context = await browser.newContext({
      httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });

    const page = await context.newPage();
    await page.goto(emailUrl);
    await page.getByRole('cell', { name: 'ACTION REQUIRED - Confirm' }).first().click();
    await page.getByRole('tab', { name: 'JSON' }).click();
    await page.waitForTimeout(5000); // Wait! 

    const preContent = await page.evaluate(() => {
      const preElement = document.querySelector('#pills-json-content pre');
      return preElement ? preElement.textContent : null;
    });

    if (preContent) {
      const confirmationLink = extractConfirmationLink(preContent)
      console.log("-- Confirmation Link: ", confirmationLink)

      await page.goto(confirmationLink);
      await page.waitForTimeout(5000) // Wait 
      await expect(page.getByRole('heading')).toContainText('It looks like you have already confirmed this appointment.');
    } else {
      console.log("<pre> Was not found");
    }
  });

  // NOTE: Test not so necessary but I prefer not to remove it. 
  test('Should MVA parameter present in URL', async ({ browser }) => {
    const context = await browser.newContext({
        httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });
    const page = await context.newPage();

    await page.goto('https://bookingqa.lifemark.ca/OnlineBooking/index?serviceId=10014&locationId=418379&brand=3&mva=true');
    await page.waitForTimeout(3000); // Wait! 

    const currentUrl = page.url();
    console.log("currentUrl: ", currentUrl)
    const hasParameter = currentUrl.includes('mva=true');
    expect(hasParameter, `URL should contain mva=true parameter`).toBe(true);

    /*
      await expect(page.getByRole('link', { name: 'Date & Time' })).toBeVisible();
      // NOTE: Be aware! This is the "position" of the Appointment's hour
      await page.waitForSelector('a.btn.btn-sm.btn-default.time-slot-button:not(.disabled)', { state: 'visible' }); // Wait

      await page.click('a.btn.btn-sm.btn-default.time-slot-button:not(.disabled)');
      // await page.locator('a.btn.btn-sm.btn-default.time-slot-button:not(.disabled)').nth(2).click();

      await page.getByLabel('First Name: *').fill('John');
      await page.getByLabel('Last Name: *').fill('Doe');
      await page.getByLabel('Email: *').fill('john@doe.com');
      await page.getByLabel('Phone: *').fill('1234561234');
      await page.getByLabel('Insurance Company: *').fill('Insurance Co.');
      await page.getByLabel('Name of Adjuster:').fill('Jim Doe');
      await page.getByLabel('Adjuster Email:').fill('jim@insurance.com');
      await page.getByLabel('Date of Accident (yyyy/mm/dd').fill('2024/01/01');
      await page.getByLabel('Claim Number:').fill('123456');
      await page.waitForTimeout(5000); // Wait! 
      await page.getByRole('link', { name: 'Request Appointment Now' }).click();
      await page.waitForTimeout(3000); // Wait! 
      await expect(page.locator('#step_final')).toContainText('Thank-you, your appointment has been booked!');
    */
  });

  test('Live chat should show up', async ({ browser }) => {
    const context = await browser.newContext({
      httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });
    const page = await context.newPage();

    await page.goto(url);

    await page.waitForTimeout(5000) // Wait 

    await expect(page.getByRole('link', { name: 'Chat with us' })).toBeVisible();
    await page.getByRole('link', { name: 'Chat with us' }).click();
    await expect(page.frameLocator('iframe[name="embedded-frame"]').getByRole('button', { name: 'Start Chat' })).toBeVisible({ timeout: 15000 });
    await expect(page.frameLocator('iframe[name="embedded-frame"]').getByLabel('Name')).toBeVisible();
    await expect(page.frameLocator('iframe[name="embedded-frame"]').getByLabel('Email', { exact: true })).toBeVisible();
    await expect(page.frameLocator('iframe[name="embedded-frame"]').getByLabel('Question')).toBeVisible();
  });

});
