import { chromium } from "playwright";
import dotenv from "dotenv";
dotenv.config();

(async () => {
  const browser = await chromium.launch({ headless: true });

  const context = await browser.newContext({
    httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
  });

  // Page 1: API de trabajos
  const page1 = await context.newPage();
  await page1.goto('https://www.careers.branchmanager.lifemark.ca/api/json/adp-jobs');

  // Page 2: Carrers page
  const page2 = await context.newPage();
  await page2.goto('https://uat.physiotherapieuniverselle.com/carriere/emplois');

  // Array to save all jobs
  const allJobInfo = [];

  // Function to extract info from the actual page.
  const extractJobInfo = async (page) => {
    return page.evaluate(() => {
      const items = Array.from(document.querySelectorAll('.list-item'));
      return items.map(item => {
        const link = item.querySelector('.career-listing-link');
        const title = link ? link.textContent.trim() : '';
        const location = item.querySelector('p') ? item.querySelector('p').textContent.trim() : '';
        return {
          href: link ? link.href : '',
          title: title,
          location: location
        };
      });
    });
  };

  // Function to check if "Next >" exists.
  const hasNextButton = async (page) => {
    return page.evaluate(() => {
      const nextButton = document.querySelector('.wp-paginate .next');
      return !!nextButton;
    });
  };

  // Iterate through all pages
  while (true) {
    // Extraer información de trabajos de la página actual
    const jobInfo = await extractJobInfo(page2);
    allJobInfo.push(...jobInfo);

    // Check if "Next >" button exists
    const nextButtonExists = await hasNextButton(page2);
    if (!nextButtonExists) break;

    // Click on "Next >"
    await page2.click('.wp-paginate .next');
    // Wait for load
    await page2.waitForLoadState('networkidle');
  }

  // Array to save not found jobs
  const notFoundJobs = [];

  // Get JobId from href
  const extractJobId = (href) => {
    const match = href.match(/jobId=(\d+)/);
    return match ? match[1] : null;
  };

  // Get JSON from the page
  const apiJson = await page1.evaluate(() => {
    return JSON.parse(document.body.innerText);
  });

  // Scan every job
  for (const job of allJobInfo) {
    const jobId = extractJobId(job.href);
    if (!jobId) continue;

    // Search jobId in the JSON
    const jobExists = apiJson.nodes.some(node => node.link.includes(`jobId=${jobId}`));

    if (!jobExists) {
      notFoundJobs.push({
        jobId: jobId,
        href: job.href,
        title: job.title,
        location: job.location
      });
    }
  }

  console.log("Jobs not found in the JSON:", notFoundJobs);

  await browser.close();
})();
