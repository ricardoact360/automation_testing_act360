// NOTE: Removed from daily pipeline due Cloudfare
import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

/* const url = 'https://physiotherapieuniverselle.com/contact' */
const url = 'https://physiotherapieuniverselle.com/contact?act_test=true'
// const url = 'https://uat.physiotherapieuniverselle.com/contact'

test('Contact Us form should work', async ({ browser }) => {
    const context = await browser.newContext({
        httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });
    const page = await context.newPage();
    await page.goto(url);

    const formFields = [
        { label: 'Name', selector: '#input_4_1', value: process.env.TESTER_FULLNAME },
        { label: 'Email Address', selector: '#input_4_2', value: process.env.TESTER_EMAIL },
        { label: 'Subject', selector: '#input_4_4', value: process.env.TESTER_SUBJECT },
        { label: 'Message', selector: '#input_4_5', value: process.env.TESTER_PHONE }
    ];

    // Check empty inputs.
    for (const { _label, selector } of formFields) {
        await page.waitForSelector(selector, { state: 'visible' });
        const locator = page.locator(selector);
        await expect(locator).toBeEmpty({ timeout: 10000 });
    }

    // Fill fields.
    for (const { selector, value } of formFields) {
        await page.fill(selector, value);
    }

    // Verify that the fields have been filled in.
    for (const { selector, value } of formFields) {
        const locator = page.locator(selector);
        await expect(locator).toHaveValue(value, { timeout: 10000 });
    }

    await page.getByText('Envoyer').click();

    await page.waitForTimeout(5000) // Wait 

    // await expect(page.locator('text="Le formulaire a bien été envoyé et nous vous reviendrons dès que possible."')).toBeVisible();
    await expect(page.getByRole('heading', { name: 'Merci de votre intérêt.' })).toBeVisible();
    // await expect(page.getByText('Le formulaire a bien été')).toBeVisible();
});
