import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

test.describe('Physiotherapieuniverselle (Fr) jobs dropdowns', { tag: '@physiotherapieuniverselle' }, () => {
  const URL = 'https://physiotherapieuniverselle.com/carriere/emplois'
  /* const URL = 'https://uat.physiotherapieuniverselle.com/carriere/emplois' */

  test('Should not have duplicate cities in dropdown (Fr)', { tag: '@physiotherapieuniverselle' }, async ({ page }) => {
    await page.goto(URL);

    // NOTE: Get all <li> inside dropdown
    const cities = await page.$$eval('.online-booking-dropdown ul li', (items) => {
      return items.map(item => item.textContent.trim());
    });

    // NOTE: Creates a set in order to delete duplicated
    const uniqueCities = new Set(cities);

    // NOTE: Checks for duplicates by comparing the length of the original array to the Set 
    const hasDuplicates = cities.length !== uniqueCities.size;

    // NOTE: If there are duplicates, find out what they are 
    const duplicates = cities.filter((city, index) => 
      cities.indexOf(city) !== index
    );

    // NOTE: Principal assertion
    expect(hasDuplicates, 
      `Found duplicate cities: ${duplicates.join(', ')}`
    ).toBeFalsy();

    // NOTE: Additional logging in case of duplicates
    if (hasDuplicates) {
      console.log('Duplicate cities found:');
      duplicates.forEach(city => {
        const indices = cities.reduce((acc, curr, i) => {
          if (curr === city) acc.push(i);
          return acc;
        }, []);
        console.log(`${city} appears at positions: ${indices.join(', ')}`);
      });
    }
  });


  test('Should not have duplicate job in dropdown (Fr)', async ({ page }) => {
    await page.goto(URL);

    const categories = await page.$$eval('#job-categories-ul li', (items) => {
      return items.map(item => {
        return item.textContent.trim().replace(/\s*\(\d+\)$/, '');
      });
    });

    const uniqueCategories = new Set(categories);
    const hasDuplicates = categories.length !== uniqueCategories.size;

    const duplicates = categories.filter((category, index) => 
      categories.indexOf(category) !== index
    );

    expect(hasDuplicates, 
      `Found duplicate categories: ${duplicates.join(', ')}`
    ).toBeFalsy();

    if (hasDuplicates) {
      console.log('Duplicate categories found:');
      duplicates.forEach(category => {
        const indices = categories.reduce((acc, curr, i) => {
          if (curr === category) acc.push(i);
          return acc;
        }, []);
        console.log(`${category} appears at positions: ${indices.join(', ')}`);
      });
    }
  });

});
