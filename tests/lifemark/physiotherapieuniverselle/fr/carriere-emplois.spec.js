import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://physiotherapieuniverselle.com/carriere/emplois'

test.describe('Physiotherapieuniverselle (Fr) Current Oportunities', { tag: '@physiotherapieuniverselle' }, () => {
  test('Selects (2) and Buttons (2) should be present', async ({ browser }) => {
    const context = await browser.newContext({
        httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });
    const page = await context.newPage();
    await page.goto(url);

    await expect(page.getByText('Choisir une option').first()).toBeVisible();
    await expect(page.getByText('Choisir une option').nth(1)).toBeVisible();
    await expect(page.getByRole('button', { name: 'Réinitialiser' })).toBeVisible();
    await expect(page.getByRole('button', { name: 'Recherche' })).toBeVisible();
  });
});

