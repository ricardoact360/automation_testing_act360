import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://physiotherapieuniverselle.com/en/clinics?qs=Drummondville'
// const url = 'https://master.dev.pthealth.ca/find-a-clinic/'  
const emailUrl = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${process.env.MAILNATOR_ID}`

async function clickRightArrowMultipleTimes(page, times) {
  let count = 0;
  while (count < times) {
    await page.click('a.btn.btn-primary.btn-sm.calendar-arrow.right');
    await page.waitForTimeout(500);
    count++;
  }
}

test('Should fill form and booking', { tag: '@physiotherapieuniverselle' }, async ({ page }) => {
  /* await page.goto(url);
  /* await page.goto(url, { waitUntil: "domcontentloaded" }); */

  test.setTimeout(100000);
  await page.goto(url, { waitUntil: 'networkidle' });
  await page.locator('.btn').first().click();
  await expect(page.locator('.btn').first()).toBeVisible();
  await page.locator('.current-select').first().click();
  await page.locator('body').getByRole('document').getByRole('list').getByText('Physiotherapy').click();
  const page1Promise = page.waitForEvent('popup');
  await page.getByRole('button', { name: 'Go', exact: true }).click();
  const page1 = await page1Promise;
  await page1.locator('#non-virtual').getByText('No', { exact: true }).click();
  await page1.locator('div').filter({ hasText: /^I'm not sure$/ }).nth(1).click();
  await page1.locator('#reason-for-visit').click();
  await page1.locator('#reason-for-visit').fill('  This is a test, please ignore');
  await page1.getByRole('link', { name: 'Continue' }).click();
  await page1.waitForTimeout(7000); // Wait! This is the "trick"

  await clickRightArrowMultipleTimes(page1, 13); // Execute function!
  await page1.getByRole('cell', { name: '3pm - 5pm' }).nth(1).click();

  await page1.getByRole('link', { name: 'Continue' }).click();
  await page1.getByLabel('First Name: *').fill(process.env.TESTER_NAME);
  await page1.getByLabel('Last Name: *').fill(process.env.TESTER_LASTNAME);
  await page1.getByLabel('Phone: *').fill(process.env.TESTER_PHONE);
  await page1.getByLabel('Email: *').fill(process.env.TESTER_EMAIL);
  await page1.getByRole('link', { name: 'Request Appointment Now' }).click();
  await expect(page1.getByRole('heading', { name: 'Thank you for requesting an' })).toBeVisible();

  await page.waitForTimeout(7000) // Wait
  await page.goto(emailUrl);
  await page.waitForTimeout(7000) // Wait
  await page.getByRole('cell', { name: 'Fw: Appointment Request for Physiotherapy with Physiothérapie Universelle' }).first().click();
  await expect(page.locator('#email_pane')).toContainText('Fw: Appointment Request for Physiotherapy with Ph...');
});
