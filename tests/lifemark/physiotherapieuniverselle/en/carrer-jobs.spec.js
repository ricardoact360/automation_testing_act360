import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://physiotherapieuniverselle.com/en/career/jobs'

test.describe('Physiotherapieuniverselle (En) Current Oportunities', { tag: '@physiotherapieuniverselle' }, () => {
  let page;
  
  // NOTE: Before
  test.beforeEach(async ({ browser }) => {
    const context = await browser.newContext({
      httpCredentials: { 
        username: process.env.LIFEMARK_USERNAME, 
        password: process.env.LIFEMARK_PASSWORD 
      }
    });
    page = await context.newPage();
    await page.goto(url);
  });

  // NOTE: Buttons are present
  test('Selects (2) and Buttons (2) should be present', async () => {
    await expect(page.getByText('Choose an option').first()).toBeVisible();
    await expect(page.getByText('Choose an option').nth(1)).toBeVisible();
    await expect(page.getByRole('button', { name: 'Reset' })).toBeVisible();
    await expect(page.getByRole('button', { name: 'Search' })).toBeVisible();
  });

  // NOTE: Jobs Quantity:
  const jobCountTests = [
    { threshold: 0, description: 'more than 1' },
    /* { threshold: 10, description: 'more than 10' }, */
    /* { threshold: 20, description: 'more than 20' }, */
    /* { threshold: 30, description: 'more than 30' }, */
    /* { threshold: 40, description: 'more than 40' } */
  ];

  for (const { threshold, description } of jobCountTests) {
    test(`Should be ${description} jobs available`, async () => {
      const openingsText = await page.locator('h3:has-text("current openings")').textContent();
      const count = parseInt(openingsText.match(/\d+/)[0]);
      expect(count).toBeGreaterThan(threshold);
    });
  }

  // NOTE: Test language pagination
  test.skip('Page buttons should work and be in English', async () => {
    // NOTE: Get job quantity
    const openingsText = await page.locator('h3:has-text("current openings")').textContent();
    const jobCount = parseInt(openingsText.match(/\d+/)[0]);

    // NOTE: If less than 10 jobs skip this test
    if (jobCount <= 10) {
      test.skip();
      return;
    }

    // NOTE: If there is more than 10 jobs execute the test (check button lenguages)
    await page.getByLabel('Go to next page').click();
    await expect(page.getByRole('link', { name: '<< First' })).toBeVisible();
    await expect(page.getByLabel('Go to previous page')).toBeVisible();
    await expect(page.getByLabel('Go to next page')).toBeVisible();
    await expect(page.getByRole('link', { name: 'Last >>' })).toBeVisible();
  });

  test('Check if the jobs number match ADP by at least 10', async () => {
    const jobCountText = await page.$eval('h3', (h3) => h3.textContent.trim());
    const jobCount = parseInt(jobCountText.match(/\d+/)[0], 10);


    const firstJobLink = await page.$eval(
      'ul.list-group li.list-item:first-child a.career-listing-link',
      (link) => link.href
    );

    await page.goto(firstJobLink);

    // NOTE: Click on "Centre de carrières"
    await page.click('ul.vdl-sidebar__menu li.vdl-sidebar-item--active a.vdl-sidebar-item__container');

    await page.waitForTimeout(5000) // Wait 

    const currentOpeningsText = await page.$eval(
      '#tileCurrentOpenings',
      (h1) => h1.textContent.trim()
    );

    // NOTE: ADP job numbers
    const adpJobNumber = parseInt(currentOpeningsText.match(/\d+/g)[1], 10);

    console.log(`Number of job offers: ${jobCount}`);
    console.log(`ADP job numbers: ${adpJobNumber}`);

    const jobDifference = adpJobNumber - jobCount;

    // NOTE: If the difference is greater than 10 don't pass
    if (jobDifference > 10) {
      // Fail
      throw new Error(`Difference between ADP job numbers and jobCount is greater than 10. Difference: ${jobDifference}`);
      // expect(jobDifference).toBeLessThanOrEqual(10);
    } else {
      //Pass
      console.log('Test passed: The difference is 10 or less.');
    }
  });

});
