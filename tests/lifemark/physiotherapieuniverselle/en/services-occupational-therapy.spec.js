import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://physiotherapieuniverselle.com/en/services/occupational-therapy'

test.describe('Physiotherapieuniverselle Services Occupational Therapy', { tag: '@physiotherapieuniverselle' }, () => {
  test('Accordion should work', async ({ browser }) => {
      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });
      const page = await context.newPage();
      await page.goto(url);
      await page.getByRole('tab', { name: 'Hand therapy' }).click();
      await expect(page.getByText('Damage to an upper limb, i.e')).toBeVisible();
      await page.getByRole('tab', { name: 'Mental health' }).click();
      await expect(page.getByText('Symptoms arising from a')).toBeVisible();
      await page.getByRole('tab', { name: 'Outdoor reactivation' }).click();
      await expect(page.getByText('Outdoor reactivation involves')).toBeVisible();
      await page.getByRole('tab', { name: 'Pediatric occupational therapy' }).click();
      await expect(page.getByText('If your child seems to have')).toBeVisible();
      await page.getByRole('tab', { name: 'Paratransit and parking' }).locator('i').first().click();
      await expect(page.getByText('A disability can generate')).toBeVisible();
      await page.getByRole('tab', { name: 'Self-help aids' }).locator('i').first().click();
      await expect(page.getByText('An occupational therapist can help you by recommending appropriate self-help')).toBeVisible();
      await page.getByRole('tab', { name: 'Somatosensory rehabilitation' }).locator('i').first().click();
      await expect(page.getByText('The purpose of somatosensory')).toBeVisible();
      await page.getByRole('tab', { name: 'Somatosensory rehabilitation' }).click();
      await expect(page.getByRole('tab', { name: 'Somatosensory rehabilitation' })).toBeVisible();  
  });
});
