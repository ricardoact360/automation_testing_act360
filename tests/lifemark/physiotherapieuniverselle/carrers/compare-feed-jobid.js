import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Reed JSON
const universalleJobInfo = JSON.parse(fs.readFileSync(path.join(__dirname, 'universelle-job-info.json'), 'utf8'));
const feedUniverselleJobs = JSON.parse(fs.readFileSync(path.join(__dirname, 'feed_universelle_jobs.json'), 'utf8'));

// Create a set of jobId from universelle-job-info
const jobIdSet = new Set(universalleJobInfo.map(job => job.jobId));

// Filter out feed_universelle_jobs nodes that do not have an associated jobId
const unassociatedJobs = feedUniverselleJobs.nodes.filter(node => {
    const jobIdFromLink = node.link.match(/jobId=(\d+)/);
    return jobIdFromLink && !jobIdSet.has(jobIdFromLink[1]);
});

// Display the results in the console
console.log("Objects in feed_universelle_jobs that do NOT have an associated jobId in universelle-job-info:");
console.log(JSON.stringify(unassociatedJobs, null, 2));
