import { chromium } from "playwright";
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import dotenv from "dotenv";
dotenv.config();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

(async () => {
  const browser = await chromium.launch({ headless: true });
  const context = await browser.newContext({
    httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
  });

  // Page: Careers page
  const page = await context.newPage();

  // NOTE: Works for FR and EN. Just change the link.
  await page.goto('https://uat.physiotherapieuniverselle.com/carriere/emplois');
  /* await page.goto('https://uat.physiotherapieuniverselle.com/en/career/jobs'); */
  /* await page.goto('https://physiotherapieuniverselle.com/en/career/jobs'); */

  // Array to save all jobs
  const allJobInfo = [];

  // Function to extract info from the actual page.
  const extractJobInfo = async (page) => {
    return page.evaluate(() => {
      const items = Array.from(document.querySelectorAll('.list-item'));
      return items.map(item => {
        const link = item.querySelector('.career-listing-link');
        const title = link ? link.textContent.trim() : '';
        const location = item.querySelector('p') ? item.querySelector('p').textContent.trim() : '';
        const href = link ? link.href : '';
        
        // Extract jobId from href
        const jobId = href.match(/jobId=(\d+)/)?.[1] || '';

        return {
          href: href,
          title: title,
          location: location,
          jobId: jobId
        };
      });
    });
  };

  // Function to check if "Next >" exists.
  const hasNextButton = async (page) => {
    return page.evaluate(() => {
      const nextButton = document.querySelector('.wp-paginate .next');
      return !!nextButton;
    });
  };

  // Iterate through all pages
  while (true) {
    // Extract job information from the current page
    const jobInfo = await extractJobInfo(page);
    allJobInfo.push(...jobInfo);

    console.log(`Extracted ${jobInfo.length} jobs from current page`);

    // Check if "Next >" button exists
    const nextButtonExists = await hasNextButton(page);
    if (!nextButtonExists) break;

    // Click on "Next >"
    await page.click('.wp-paginate .next');
    // Wait for load
    await page.waitForLoadState('networkidle');
  }

  console.log(`Total number of jobs: ${allJobInfo.length}`);

  const fileName = 'universelle-job-info.json';
  const filePath = path.join(__dirname, fileName);

  // Save the result as a JSON file
  fs.writeFileSync(filePath, JSON.stringify(allJobInfo, null, 2));

  console.log("Results saved to universelle-job-info.json");

  await browser.close();
})();
