import { chromium } from "playwright";
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import dotenv from "dotenv";
dotenv.config();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

(async () => {
  const browser = await chromium.launch({ headless: true });
  const context = await browser.newContext({
    httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
  });

  const page = await context.newPage();
  const invalidJobs = [];
  let hasNextPage = true;
  let pageNumber = 1;

  while (hasNextPage) {
    const currentPageUrl = `https://uat.physiotherapieuniverselle.com/carriere/emplois/page/${pageNumber}`;
    /* const currentPageUrl = `https://physiotherapieuniverselle.com/en/career/jobs/page/${pageNumber}`; */
    await page.goto(currentPageUrl);
    console.log(`Checking page ${pageNumber}`);

    // Get all job links
    const jobLinks = await page.evaluate(() => {
      const links = Array.from(document.querySelectorAll('.career-listing-link'));
      return links.map(link => ({
        title: link.textContent.trim(),
        href: link.href
      }));
    });

    // Check each job link
    for (const job of jobLinks) {
      const jobPage = await context.newPage();
      await jobPage.goto(job.href);

      await jobPage.waitForTimeout(2500); // Wait 

      // NOTE: Check if has "We are no longer accepting applications for this position" message exists
      const hasJobDescriptionTitle = await jobPage.evaluate(() => {
        return document.querySelector('.job-description-title') !== null;
      });

      // Check if ccId parameter is equal to 19000101_000001
      const url = new URL(job.href);
      const ccId = url.searchParams.get('ccId');
      const isIncorrectBrand = ccId === '19000101_000001';

      let message = "";

      if (!hasJobDescriptionTitle && isIncorrectBrand) {
        message = "Job not available & Incorrect brand";
      } else if (!hasJobDescriptionTitle) {
        message = "Job not available";
      } else if (isIncorrectBrand) {
        message = "Incorrect brand";
      }

      if (message) {
        invalidJobs.push({
          ...job,
          message: message,
          sourcePageUrl: currentPageUrl
        });
      }

      await jobPage.close();
    }

    // Check if there's a next page
    hasNextPage = await page.evaluate(() => {
      const nextButton = document.querySelector('.wp-paginate .next');
      const lastButton = document.querySelector('.wp-paginate a:last-child');
      return nextButton !== null && !lastButton.classList.contains('current');
    });

    if (hasNextPage) {
      pageNumber++;
    }
  }

  // Save invalid jobs to JSON file
  const jsonContent = JSON.stringify(invalidJobs, null, 2);
  fs.writeFileSync(path.join(__dirname, 'errors-jobs-universelle.json'), jsonContent);

  console.log(`Found ${invalidJobs.length} invalid jobs across ${pageNumber} pages. Results saved to errors-jobs.json`);

  await browser.close();
})();
