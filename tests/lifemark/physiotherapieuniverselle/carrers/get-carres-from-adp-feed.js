import { chromium } from "playwright";
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import dotenv from "dotenv";
dotenv.config();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const language = 'fr'; // Language 'en', 'fr'

(async () => {
  const browser = await chromium.launch({ headless: true });
  const context = await browser.newContext({
    httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
  });

  // Page: Careers page
  const page1 = await context.newPage();
  const page2 = await context.newPage();

  // TODO: --- ALWAYS Ask if compares agaist "careers.branchmanager" or "lifemark.ca" ---
  /* await page1.goto('https://www.careers.branchmanager.lifemark.ca/api/json/adp-jobs'); */
  /* await page2.goto('https://www.careers.branchmanager.lifemark.ca/api/json/adp-jobs?page=1'); */
  await page1.goto('https://www.lifemark.ca/api/json/adp-jobs');
  await page2.goto('https://www.lifemark.ca/api/json/adp-jobs?page=1');

  // Get JSON content from both pages
  const json1 = await page1.evaluate(() => JSON.parse(document.body.textContent));
  const json2 = await page2.evaluate(() => JSON.parse(document.body.textContent));

  // Merge the JSON data and filter by language
  const filteredNodes = [...json1.nodes, ...json2.nodes].filter(node => 
    node.title.endsWith(`_${language}_CA`)
  );

  const mergedJson = {
    nodes: filteredNodes,
    pager: {
      pages: Math.max(json1.pager.pages, json2.pager.pages),
      page: 0,
      count: filteredNodes.length,
      limit: json1.pager.limit
    }
  };

  // Write the merged JSON to a file
  const outputPath = path.join(__dirname, 'feed_universelle_jobs.json');
  fs.writeFileSync(outputPath, JSON.stringify(mergedJson, null, 2));

  console.log(`Filtered and merged JSON has been saved to ${outputPath}`);

  await browser.close();
})();
