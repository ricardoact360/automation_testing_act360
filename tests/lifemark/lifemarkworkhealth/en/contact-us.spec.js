import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://www.lifemarkworkhealth.ca/contact-us'

test.use({ storageState: "./auth/auth-lifemarkworkhealth.json" })
test.describe('Lifemarkworkhealth Contac Us English', { tag: '@lifemarkworkhealth' }, () => {
  test('Contact Us (English) form should work', async ({ page }) => {
      await page.goto(url);

      // Press radio
      await page.check('#edit-are-you-a-new-or-existing-client-new');

      const formFields = [
          { label: 'Company Name', selector: '#edit-subject', value: 'Act 360' },
          { label: 'Contact person first name', selector: '#edit-contact-person-first-name', value: process.env.TESTER_NAME },
          { label: 'Contact person last name', selector: '#edit-contact-person-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Contact person title', selector: '#edit-title', value: process.env.TESTER_SUBJECT },
          { label: 'Contact person phone number', selector: '#edit-telephone', value: process.env.TESTER_PHONE},
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'Message', selector: '#edit-message', value: process.env.TESTER_MESSAGE }
      ];

      // How can we help you?
      await page.selectOption('#edit-how-can-i-help-you-', 'General Inquires');

      // Check empty inputs.
      for (const { _label, selector } of formFields) {
          await page.waitForSelector(selector, { state: 'visible' });
          const locator = page.locator(selector);
          await expect(locator).toBeEmpty({ timeout: 10000 });
      }

      // Fill fields.
      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      // Verify that the fields have been filled in.
      for (const { selector, value } of formFields) {
          const locator = page.locator(selector);
          await expect(locator).toHaveValue(value, { timeout: 10000 });
      }

      await page.click('#edit-actions-submit');

      await page.waitForTimeout(10000) // Wait 

      await expect(page.locator('text="Thank you for the form submission!"')).toBeVisible();
  });

  test('Should be cleared all inputs when Clear Form button is pressed.', async ({ page }) => {
      await page.goto('https://www.lifemarkworkhealth.ca/contact-us');

      // Press radio
      await page.check('#edit-are-you-a-new-or-existing-client-new');

      const formFields = [
          { label: 'Company Name', selector: '#edit-subject', value: 'Act 360' },
          { label: 'Contact person first name', selector: '#edit-contact-person-first-name', value: process.env.TESTER_NAME },
          { label: 'Contact person last name', selector: '#edit-contact-person-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Contact person title', selector: '#edit-title', value: process.env.TESTER_SUBJECT },
          { label: 'Contact person phone number', selector: '#edit-telephone', value: process.env.TESTER_PHONE},
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'Message', selector: '#edit-message', value: process.env.TESTER_MESSAGE }
      ];

      // How can we help you?
      await page.selectOption('#edit-how-can-i-help-you-', 'General Inquires');

      // Check empty inputs.
      for (const { _label, selector } of formFields) {
          await page.waitForSelector(selector, { state: 'visible' });
          const locator = page.locator(selector);
          await expect(locator).toBeEmpty({ timeout: 10000 });
      }

      // Fill fields.
      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      // Verify that the fields have been filled in.
      for (const { selector, value } of formFields) {
          const locator = page.locator(selector);
          await expect(locator).toHaveValue(value, { timeout: 10000 });
      }

      await page.click('#edit-actions-custom-reset');


      // Check empty inputs.
      for (const { _label, selector } of formFields) {
          await page.waitForSelector(selector, { state: 'visible' });
          const locator = page.locator(selector);
          await expect(locator).toBeEmpty({ timeout: 10000 });
      }
  });
});
