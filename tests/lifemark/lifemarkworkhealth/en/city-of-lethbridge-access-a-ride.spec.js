import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://www.lifemarkworkhealth.ca/city-of-lethbridge-access-a-ride'
/* const url = 'https://upcore.branchmanager.lifemarkworkhealth.ca/city-of-lethbridge-access-a-ride' */
const emailUrl = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${process.env.MAILNATOR_ID}`

test.use({ storageState: "./auth/auth-lifemarkworkhealth.json" })
test.describe('Lifemarkworkhealth Secure document upload', { tag: '@lifemarkworkhealth' }, () => {
  test('Secure document upload form should work', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });

      const page = await context.newPage();
      await page.goto(url);

      await page.getByRole('link', { name: 'Secure document upload' }).click();

      const formFields = [
          { label: 'Contact person first name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
          { label: 'Contact person last name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Full name', selector: '#edit-form-submitted-by-full-name-', value: process.env.TESTER_FULLNAME },
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'Contact person phone number', selector: '#edit-phone-number', value: process.env.TESTER_PHONE},
          { label: 'Message', selector: '#edit-comments', value: process.env.TESTER_MESSAGE }
      ];

      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      await page.getByText('Choose Files').click();
      await page.getByLabel('Choose Files').setInputFiles('assets/files/example_text.txt');
      await page.waitForTimeout(7000)
      await expect(page.getByText(/example_text.*\.txt/)).toBeVisible();

      await page.click('#edit-submit');

      await expect(page.locator('text="Thank you for your submission!"')).toBeVisible();

      await page.goto(emailUrl);
      await page.waitForTimeout(7000)

      await expect(page.getByRole('cell', { name: 'Workplace Health & Wellness' }).first()).toBeVisible();
      await expect(page.getByRole('cell', { name: 'Access-A-Ride Application' }).first()).toBeVisible();
  });

  test('Should show error if try to upload a PHP file', async ({ browser }) => {
      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });
      const page = await context.newPage();
      await page.goto(url);
      await page.getByRole('link', { name: 'Secure document upload' }).click();
      await page.getByText('Choose Files').click();
      await page.getByLabel('Choose Files').setInputFiles('assets/files/example_php.php');
      await page.waitForTimeout(7000)
      await expect(page.locator('li', { hasText: 'Only files with the following extensions are allowed:' }).first()).toBeVisible();
  });

  test('Should show error if try to upload a JS file', async ({ browser }) => {
      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });
      const page = await context.newPage();
      await page.goto(url);
      await page.getByRole('link', { name: 'Secure document upload' }).click();
      await page.getByText('Choose Files').click();
      await page.getByLabel('Choose Files').setInputFiles('assets/files/example_js.js');
      await page.waitForTimeout(7000)
      await expect(page.locator('li', { hasText: 'Only files with the following extensions are allowed:' }).first()).toBeVisible();
  });

  test('Should show error if try to upload a JSP file', async ({ browser }) => {
      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });
      const page = await context.newPage();
      await page.goto(url);
      await page.getByRole('link', { name: 'Secure document upload' }).click();
      await page.getByText('Choose Files').click();
      await page.getByLabel('Choose Files').setInputFiles('assets/files/example_jsp.jsp');
      await page.waitForTimeout(7000)
      await expect(page.locator('li', { hasText: 'Only files with the following extensions are allowed:' }).first()).toBeVisible();
  });

  test('Should it able to remove uploaded file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
      const page = await context.newPage();

      await page.goto(url);
      await page.getByRole('link', { name: 'Secure document upload' }).click();

      await page.getByText('Choose Files').click();
      await page.getByLabel('Choose Files').setInputFiles('assets/files/example_text.txt');
      await page.waitForTimeout(7000) // Wait for upload files
      await expect(page.getByText(/example_text(_\d+)?\.txt/).first()).toBeVisible();
      await page.locator('[id*="edit-add-a-new-file--"] span').nth(1).check();
      await page.getByRole('button', { name: 'Remove selected' }).click();
      await expect(page.getByText(/example_text(_\d+)?\.txt/).first()).not.toBeVisible();
  });
});
