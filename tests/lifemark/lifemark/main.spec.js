import { test, expect } from "@playwright/test";

const url = 'https://www.lifemark.ca/'

test.use({
  viewport: { height: 800, width: 360 }
});

test.describe('Lifemark Homepage', { tag: '@lifemark' }, () => {
  test('Accordion should work', async ({ page }) => {
    await page.goto(url);
    await page.getByRole('heading', { name: 'Specialty programs ' }).click();
    await expect(page.getByRole('heading', { name: 'Dizziness + balance' })).toBeVisible();
    await expect(page.getByRole('heading', { name: 'Pelvic health' })).toBeVisible();
    await expect(page.getByRole('heading', { name: 'Mental health' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Concussion care' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Cancer rehab' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Post COVID-' })).toBeVisible();
    await page.getByRole('heading', { name: 'Additional services ' }).click();
    await expect(page.getByRole('link', { name: 'Vocational rehabilitation' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Pain management' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Acupuncture' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Orthotics + pedorthics' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Bracing' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Sport Medicine' })).toBeVisible();
  });
});
