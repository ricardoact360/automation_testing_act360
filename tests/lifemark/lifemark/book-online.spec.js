import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://www.lifemark.ca/book-online'

test.describe('Lifemark Booking', { tag: '@lifemark' }, () => {
  test('Book online form should work', async ({ page }) => {
      await page.goto(url, { waitUntil: "domcontentloaded" });

      await page.getByPlaceholder('Enter City, Province, Postal').fill('Toronto');
      await page.getByLabel('Choose a service *').selectOption('10003');
      await page.locator('#location-gmap-form').getByRole('button', { name: 'Search' }).click();

      await page.getByRole('link', { name: 'Select' }).first().click();

      // Press radio
      await page.check('input[name="virtualSelection"][value="2"]');

      await page.waitForTimeout(5000) // Wait 

      const formFields = [
          { label: 'First Name', selector: '#edit-submitted-first-name', value: process.env.TESTER_NAME },
          { label: 'Last Name', selector: '#edit-submitted-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Email', selector: '#edit-submitted-email-address', value: process.env.TESTER_EMAIL },
          { label: 'Phone number', selector: '#edit-submitted-phone-number', value: process.env.TESTER_PHONE },
          { label: 'Postal code', selector: '#edit-submitted-postal-code', value: 'A9A9A9' },
          { label: 'Message', selector: '#edit-submitted-message', value: process.env.TESTER_MESSAGE }
      ];
      await page.waitForSelector('#virtual-service-frame iframe');
      const iframe = page.frameLocator('#virtual-service-frame iframe');

      for (const { selector, value } of formFields) {
        /* await page.frameLocator('iframe').locator(selector).waitFor({ state: 'visible' }); */
        /* await page.frameLocator('iframe').locator(selector).fill(value); */
        await iframe.locator(selector).waitFor({ state: 'visible' });
        await iframe.locator(selector).fill(value);
      }

      await iframe.getByLabel('Month').selectOption('6');
      await iframe.getByLabel('Day').selectOption('5');
      await iframe.getByLabel('Year').selectOption('2026');

      await iframe.getByLabel('Preferred time').selectOption('afternoon');
      await iframe.getByLabel('Select a service').selectOption('ergonomic');

      await page.waitForTimeout(3000) // Wait 

      await iframe.getByRole('button', { name: 'Request appointment' }).click();

      await page.waitForTimeout(7000) // Wait 

      await expect(iframe.getByRole('heading')).toContainText('Thank you for your request');
  });

  test.skip('Should show alert when search with no parameters', async ({ page }) => {
      await page.goto(url, { waitUntil: "domcontentloaded" });

      // Note: this should be always before the action
      page.on('dialog', async dialog => {
        expect(dialog.type()).toBe('alert');
        expect(dialog.message()).toBe('Address and service field required');
        await dialog.accept();
      });

      await page.locator('#location-gmap-form').getByRole('button', { name: 'Search' }).click();
  });
});
