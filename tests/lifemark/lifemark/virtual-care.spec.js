import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://www.lifemark.ca/services/virtual-care'
// const url = 'https://www.upcore.branchmanager.vp-group.ca/contact-us'

test.describe('Lifemar Book a Virtual Care Appointment', { tag: '@lifemark' }, () => {
  test('Book a virtual care appointment should work', async ({ browser }) => {
    const context = await browser.newContext({
        httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });
    const page = await context.newPage();
    await page.goto(url, { waitUntil: "domcontentloaded" }); // Don't remove this. SOLUTION!

    const formFields = [
        { label: 'First Name', selector: '#edit-submitted-first-name', value: process.env.TESTER_NAME },
        { label: 'Last Name', selector: '#edit-submitted-last-name', value: process.env.TESTER_LASTNAME },
        { label: 'Phone number', selector: '#edit-submitted-phone-number', value: process.env.TESTER_PHONE },
        { label: 'Email', selector: '#edit-submitted-email-address', value: process.env.TESTER_EMAIL },
        { label: 'Postal Code', selector: '#edit-submitted-postal-code', value: 'A9A9A9' },
        { label: 'Message', selector: '#edit-submitted-message', value: process.env.TESTER_MESSAGE }
    ];

    for (const { selector, value } of formFields) {
        await page.fill(selector, value);
    }

    await page.getByLabel('Month').selectOption('7');
    await page.getByLabel('Day').selectOption('11');
    await page.getByLabel('Year').selectOption('2026');
    await page.getByLabel('Preferred time').selectOption('anytime');
    await page.getByLabel('Select a service').selectOption('concussion');

    await page.waitForTimeout(10000)

    await page.getByRole('button', { name: 'Request appointment' }).click();

    await page.waitForTimeout(10000)

    await expect(page.getByRole('heading', { name: 'Thank you for your request' })).toBeVisible();
  });

  test('Fields required message should work', async ({ browser }) => {
    const context = await browser.newContext({
        httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });
    const page = await context.newPage();
    await page.goto(url, { waitUntil: "domcontentloaded" }); // Don't remove this. SOLUTION!
    await page.waitForTimeout(10000)


    await page.getByRole('button', { name: 'Request appointment' }).click();

    await expect(page.getByText('First name field is required.')).toBeVisible();
    await expect(page.getByText('Last name field is required.')).toBeVisible();
    await expect(page.getByText('Email address field is')).toBeVisible();
    await expect(page.getByText('Phone number field is')).toBeVisible();
    await expect(page.getByText('Postal code field is required.')).toBeVisible();
  });
});

