import { chromium } from "playwright";
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import dotenv from "dotenv";
dotenv.config();

// Get current directory
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

(async () => {
  const browser = await chromium.launch({ headless: true });
  const context = await browser.newContext({
    httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
  });

  const page = await context.newPage();
  const baseUrl = 'https://www.lifemark.ca/current-opportunities';
  let currentPage = 0;
  let hasNextPage = true;
  let allJobs = [];

  while (hasNextPage) {
    const url = currentPage === 0 ? baseUrl : `${baseUrl}?page=${currentPage}`;
    await page.goto(url);

    // Extract job links and IDs
    const jobs = await page.evaluate(() => {
      const jobItems = document.querySelectorAll('.item-list ul li');
      return Array.from(jobItems).map(item => {
        const link = item.querySelector('.views-field-field-link a');
        const href = link ? link.getAttribute('href') : null;
        const jobId = href ? new URL(href).searchParams.get('jobId') : null;
        return { href, jobId };
      });
    });

    // Add all jobs without filtering
    allJobs = allJobs.concat(jobs);

    // Check if there's a next page
    hasNextPage = await page.evaluate(() => {
      const nextButton = document.querySelector('.pagination .next a');
      return !!nextButton;
    });

    if (hasNextPage) {
      currentPage++;
      console.log(`Moving to page ${currentPage + 1}`);
    }
  }

  // Save all jobs to JSON file in current directory
  const outputPath = path.join(__dirname, 'all_jobs.json');
  fs.writeFileSync(outputPath, JSON.stringify(allJobs, null, 2));

  console.log(`Saved ${allJobs.length} jobs to ${outputPath}`);

  await browser.close();
})();
