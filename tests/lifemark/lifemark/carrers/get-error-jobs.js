import { chromium } from "playwright";
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import dotenv from "dotenv";
dotenv.config();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

(async () => {
  const browser = await chromium.launch({ headless: true });
  const context = await browser.newContext({
    httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
  });

  const page = await context.newPage();
  const invalidJobs = [];
  let totalJobs = 0;
  let currentPage = 0;

  const currentPageUrl = 'https://www.lifemark.ca/current-opportunities';
  await page.goto(currentPageUrl, { waitUntil: 'networkidle' });

  async function checkJob(href, title) {
    const jobPage = await context.newPage();
    try {
      await jobPage.goto(href, { waitUntil: 'networkidle' });
      await jobPage.waitForTimeout(4000); // Wait 
      
      const hasJobDescriptionTitle = await jobPage.evaluate(() => {
        return document.querySelector('.job-description-title') !== null;
      });

      const url = jobPage.url();
      const ccId = new URL(url).searchParams.get('ccId');
      const isIncorrectBrand = ccId !== '19000101_000001';

      let message = "";

      if (!hasJobDescriptionTitle && isIncorrectBrand) {
        message = "Job not available & Incorrect brand";
      } else if (!hasJobDescriptionTitle) {
        message = "Job not available";
      } else if (isIncorrectBrand) {
        message = "Incorrect brand";
      }

      if (message) {
        invalidJobs.push({
          title,
          url,
          message
        });
      }
    } catch (error) {
      console.error(`Error processing job ${title}: ${error.message}`);
    } finally {
      await jobPage.close();
    }
  }

  async function processPage() {
    console.log(`Scanning page ${currentPage}`);
    
    // Wait for job listings to load
    await page.waitForSelector('.views-row');
    
    // Get all jobs in current page
    const jobLinks = await page.$$eval('.views-field-field-link a', links => 
      links.map(link => ({
        href: link.href,
        title: link.textContent.trim()
      }))
    );

    totalJobs += jobLinks.length;

    // Process jobs in parallel, but limit concurrency to 5
    const batchSize = 7;
    for (let i = 0; i < jobLinks.length; i += batchSize) {
      const batch = jobLinks.slice(i, i + batchSize);
      await Promise.all(batch.map(job => checkJob(job.href, job.title)));
    }

    // Check for next page
    const hasNextPage = await page.$('a[title="Go to next page"]');
    if (hasNextPage) {
      await page.click('a[title="Go to next page"]');
      await page.waitForLoadState('networkidle');
      currentPage++;
      await processPage();
    }
  }

  try {
    await processPage();
  } catch (error) {
    console.error('Error in main process:', error);
  } finally {
    fs.writeFileSync(path.join(__dirname, 'errors-jobs-lifemark.json'), JSON.stringify(invalidJobs, null, 2));
    console.log(`Found ${invalidJobs.length} invalid jobs out of ${totalJobs} total jobs reviewed. Results saved to errors-jobs-lifemark.json`);
    await browser.close();
  }
})();
