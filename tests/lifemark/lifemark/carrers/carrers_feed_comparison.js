import { chromium } from "playwright";
import dotenv from "dotenv";
dotenv.config();

// WIP: Work in progress
(async () => {
  const browser = await chromium.launch({ headless: true });

  const context = await browser.newContext({
    httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
  });


  const page1 = await context.newPage();
  await page1.goto('https://www.lifemark.ca/current-opportunities');

  await browser.close();
})();

