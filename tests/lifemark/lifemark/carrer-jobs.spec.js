import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://www.lifemark.ca/current-opportunities'

test.describe('Lifemark Current Oportunities', { tag: '@lifemark' }, () => {
  let page;
  
  // NOTE: Before
  test.beforeEach(async ({ browser }) => {
    const context = await browser.newContext({
      httpCredentials: { 
        username: process.env.LIFEMARK_USERNAME, 
        password: process.env.LIFEMARK_PASSWORD 
      }
    });
    page = await context.newPage();
    await page.goto(url);
  });

  // NOTE: Buttons are present
  test('Selects (2) and Buttons (2) should be present', async () => {
    await expect(page.locator('select[data-facet-name="field_job_location_term%3Afield_title"]')).toBeVisible();
    await expect(page.locator('select[data-facet-name="field_job_city"]')).toBeVisible();
    await expect(page.locator('select[data-facet-name="field_job_code%3Afield_category"]')).toBeVisible();
    await expect(page.getByLabel('Reset')).toBeVisible();
  });

  // NOTE: Test items jobs, if more than 0 are jobs in page.
  test('Jobs <li> should be present in page', async ({ page }) => {
    await page.goto(url);
    const listItems = await page.locator('li[class^="views-row"]').count();
    expect(listItems).toBeGreaterThan(0);
  });

  // NOTE: Test paginations
  test('Page buttons should work', async () => {
    await page.goto('https://www.lifemark.ca/current-opportunities');
    await page.getByLabel('Page next ›').click();
    await expect(page.getByRole('link', { name: '« first' })).toBeVisible();
    await expect(page.getByRole('link', { name: '‹ previous' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'next ›' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'last »' })).toBeVisible();
  });

  test('Check if the jobs number match ADP by at least 10', async () => {
    const jobCountText = await page.$eval('h3', (h3) => h3.textContent.trim());
    const jobCount = parseInt(jobCountText.match(/\d+/)[0], 10);

    const firstJobLink = await page.$eval(
      'li.views-row-1 div.views-field-field-link a',
      (link) => link.href
    );

    await page.goto(firstJobLink);

    // NOTE: Click on "Centre de carrières"
    await page.click('ul.vdl-sidebar__menu li.vdl-sidebar-item--active a.vdl-sidebar-item__container');

    await page.waitForTimeout(5000) // Wait 

    const currentOpeningsText = await page.$eval(
      '#tileCurrentOpenings',
      (h1) => h1.textContent.trim()
    );

    // NOTE: ADP job numbers
    const adpJobNumber = parseInt(currentOpeningsText.match(/\d+/g)[1], 10);

    console.log(`Number of job offers: ${jobCount}`);
    console.log(`ADP job numbers: ${adpJobNumber}`);

    const jobDifference = adpJobNumber - jobCount;

    // NOTE: If the difference is greater than 10 don't pass
    if (jobDifference > 10) {
      // Fail
      throw new Error(`Difference between ADP job numbers and jobCount is greater than 10. Difference: ${jobDifference}`);
      // expect(jobDifference).toBeLessThanOrEqual(10);
    } else {
      //Pass
      console.log('Test passed: The difference is 10 or less.');
    }
  });

});

