import { test, expect } from "@playwright/test";

const url = 'https://www.lifemark.ca/services/pelvic-health'

test.describe('Lifemark Services: Pelvic Health', { tag: '@lifemark' }, () => {
  test('Accordion should work', async ({ page }) => {
    await page.goto(url);
    await page.getByRole('heading', { name: 'Alberta ' }).click();
    await expect(page.getByRole('link', { name: 'Active Physio Works - St. Albert Servus Place' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Active Physio Works - St. Albert Tudor Glen' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Active Physio Works - Sturgeon Medical' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Alta-Sask Wellness - Cold Lake' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Lake' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Max' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Quarry' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Synergy' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Sport Medicine MNP' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Castleridge' })).toBeVisible();

    await page.getByRole('heading', { name: 'British Columbia ' }).click();
    await expect(page.getByRole('link', { name: 'Aquatic Centre Physiotherapy' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Highgate Health - pt Health' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Karp Rehabilitation - Fitness' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Motion Physiotherapy - pt' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Synergy Health Centre' }).first()).toBeVisible();
    await expect(page.getByRole('link', { name: 'Synergy Health Centre' }).nth(1)).toBeVisible();
    await page.getByRole('heading', { name: 'Newfoundland and Labrador ' }).click();
    await expect(page.getByRole('link', { name: 'Avalon Physiotherapy -' })).toBeVisible();

    await page.getByRole('heading', { name: 'Nova Scotia ' }).click();
    await expect(page.getByRole('link', { name: 'Greenwood Physiotherapy - pt' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy New' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Portland Street' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Tantallon' })).toBeVisible();

    await page.getByRole('heading', { name: 'Ontario ' }).click();
    await expect(page.getByRole('link', { name: 'Advance Physiotherapy - pt' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Bowmanville Physiotherapy and' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Central Scarborough' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Grand River Physiotherapy - Tower Street' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Harvester Road Physiotherapy' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Adaptive Health Care' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Collingwood Sport' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy &' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Bay &' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy North' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Tecumseh' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Westmount' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'NeuroLogic Physiotherapy -' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Park Road Physiotherapy &' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Pro Physio & Sport Medicine Centres Body Works Plus' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Pro Physio & Sport Medicine Centres Jeanne d\'Arc' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Blackburn & Taylor Kidd' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Brock' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Downtown London' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Fisherman & Hurontario' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Hamilton Mountain' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Hespeler Road' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'The Sports Medicine' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Toronto Physiotherapy Yonge' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Main' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Mohawk' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Trilogy Physiotherapy West' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Victoria Community Physical' })).toBeVisible();

    await page.getByRole('heading', { name: 'Nova Scotia ' }).click();
    await expect(page.getByRole('link', { name: 'Greenwood Physiotherapy - pt' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy New' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Portland Street' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark Physiotherapy Tantallon' })).toBeVisible();

    await page.getByRole('heading', { name: 'Saskatchewan ' }).click();
    await expect(page.getByRole('link', { name: 'Alta-Sask Wellness - Meadow' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Regina Sports & Physiotherapy' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Lifemark 8th Street' })).toBeVisible();
  });
});

