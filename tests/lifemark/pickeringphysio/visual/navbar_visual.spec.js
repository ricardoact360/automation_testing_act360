import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

test('Navbar should not be repeated in mobile', { tag: '@pickeringphysio' }, async ({ browser }) => {
  const url = 'https://www.pickeringphysio.ca/shockwave-therapy/';

  const context = await browser.newContext({ 
    httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD },
    viewport: { width: 1280, height: 800 }
  });
  const page = await context.newPage();
  await page.goto(url);

  await page.waitForTimeout(12000);

});
