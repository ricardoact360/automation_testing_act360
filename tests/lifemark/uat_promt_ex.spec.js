import { test, expect } from "@playwright/test";

// NOTE: This is how you set username and password for websites with protection
test('Auth and nav', async ({ browser }) => {
  const context = await browser.newContext({
    httpCredentials: {
      username: 'username',
      password: 'password'
    }
  });
  const page = await context.newPage();
  await page.goto('MY_WEBSITE');
  await expect(page).toHaveURL('MY_WEBSITE');
});
