import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://www.pthealth.ca/find-a-clinic/';

test.describe('PtHealth Search Clinics', { tag: '@pthealth' }, () => {
  let page;

  test.beforeEach(async ({ browser }) => {
    const context = await browser.newContext({
      httpCredentials: {
        username: process.env.LIFEMARK_USERNAME,
        password: process.env.LIFEMARK_PASSWORD
      }
    });
    page = await context.newPage();
    await page.goto(url, { waitUntil: "domcontentloaded" });
    console.log(`URL IN USE: ${url}`);
  });

  test('Should able to search and find clinics if search "Metrotown, Burnaby, BC, Canada"', async () => {
    const searchInput = page.getByPlaceholder('Enter your city, address or');
    
    await searchInput.fill('Metrotown, Burnaby, BC, Canada');
    await searchInput.press('Enter');
    await page.waitForTimeout(7000);

    const expectedClinics = [
      'Highgate Health - pt Health',
      'Burnaby Physiotherapy and Hand Therapy - pt Health',
      'Broadway Wellness and Physiotherapy - pt Health'
    ];

    for (const clinic of expectedClinics) {
      await expect(page.getByRole('link', { name: clinic, exact: true })).toBeVisible();
    }
  });

  test('Should able to search and find clinics if search "Mississauga, ON, Canada"', async () => {
    const searchInput = page.getByPlaceholder('Enter your city, address or');
    
    await searchInput.fill('Mississauga, ON, Canada');
    await searchInput.press('Enter');
    await page.waitForTimeout(7000);

    const expectedClinics = [
      'Six Points Physiotherapy And Rehabilitation - pt Health',
      'Four Seasons Physiotherapy - pt Health',
      'Trafalgar Physiotherapy - pt Health'
    ];

    for (const clinic of expectedClinics) {
      await expect(page.getByRole('link', { name: clinic, exact: true })).toBeVisible();
    }
  });

  test('Should able to search and find clinics if search "Barrie"', async () => {
    const searchInput = page.getByPlaceholder('Enter your city, address or');
    
    await searchInput.fill('Barrie');
    await searchInput.press('Enter');
    await page.waitForTimeout(8000);

    const expectedClinics = [
      'pt Health - Barrie Physiotherapy',
    ];

    for (const clinic of expectedClinics) {
      await expect(page.getByRole('link', { name: clinic, exact: true })).toBeVisible();
    }
  });

  test('Should able to search and not find clinics if search "Medellin"', async () => {
    const searchInput = page.getByPlaceholder('Enter your city, address or');

    await searchInput.fill('Medellin');
    await searchInput.press('Enter');
    await page.waitForTimeout(7000);

    const clinicForm = page.locator('#find-a-clinic-form');
    await expect(clinicForm).toContainText('Sorry, we could not find any clinics in your area');
    await expect(clinicForm).toContainText('We apologize, but at this time pt Health does not service your area.');
  });

  test('Should able to search and not find clinics if search "asdf"', async () => {
    const searchInput = page.getByPlaceholder('Enter your city, address or');

    await searchInput.fill('Medellin');
    await searchInput.press('Enter');
    await page.waitForTimeout(7000);

    const clinicForm = page.locator('#find-a-clinic-form');
    await expect(clinicForm).toContainText('Sorry, we could not find any clinics in your area');
    await expect(clinicForm).toContainText('We apologize, but at this time pt Health does not service your area.');
  });
});
