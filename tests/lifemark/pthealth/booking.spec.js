import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://www.pthealth.ca/find-a-clinic/' 
// const url = 'https://master.dev.pthealth.ca/find-a-clinic/'  
const emailUrl = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${process.env.MAILNATOR_ID}`

test.describe('PtHealth Booking', { tag: '@pthealth' }, () => {
  test('Should fill form and booking', async ({ browser }) => {
    console.log(`URL IN USE: ${url}`)

    const context = await browser.newContext({
        httpCredentials: {
            username: process.env.LIFEMARK_USERNAME,
            password: process.env.LIFEMARK_PASSWORD
        }
    });
    const page = await context.newPage();
    await page.goto(url, { waitUntil: "domcontentloaded" }); // Don't remove this. SOLUTION!

    await page.getByPlaceholder('Enter your city, address or postal code.').fill('Toronto');
    await page.getByRole('button', { name: 'Find' }).click();

    await page.waitForTimeout(5000) // Wait 

    await page.getByRole('link', { name: 'Select Location' }).first().click();
    await page.getByRole('link', { name: 'BOOK ONLINE', exact: true }).click();

    await page.getByRole('radio', { name: 'No' }).check();
    
    await page.getByText('I\'m not sure', { exact: true }).click();

    const textarea = page.locator('#reason-for-visit');

    await textarea.fill(process.env.TESTER_MESSAGE);

    const continueButton = page.getByRole('link', { name: 'Continue' })

    await continueButton.click()

    await expect(page.locator('#step_3')).toContainText('Pick up to three time blocks that work for you from the calendar below. We will do our best to schedule your appointment during one of these blocks. We will contact you within 24 hours to confirm your appointment time based on your request. If you require confirmation sooner, please contact the clinic during regular business hours.');

    // await page.getByRole('cell', { name: ' Jul 29, 2024 - Jul 30, 2024' }).locator('a').nth(1).click(); //here
    await page.locator('a.btn.btn-primary.btn-sm.calendar-arrow.right').click(); //HERE
    await page.locator('tr:nth-child(4) > td:nth-child(7) > .relative > .table-availability-picker-in-block-label').click();
    await page.getByRole('link', { name: 'Continue' }).click();

    await page.getByLabel('First Name: *').click();
    await page.getByLabel('First Name: *').fill(process.env.TESTER_NAME);
    await page.getByLabel('Last Name: *').click();
    await page.getByLabel('Last Name: *').fill(process.env.TESTER_LASTNAME);

    await page.getByLabel('Phone: *').click();
    await page.getByLabel('Phone: *').fill(process.env.TESTER_PHONE);

    await page.getByLabel('Email: *').click();
    await page.getByLabel('Email: *').fill(process.env.TESTER_EMAIL);

    await page.getByRole('link', { name: 'Request Appointment Now' }).click();
    await expect(page.locator('#step_5')).toContainText('Thank you for requesting an appointment');

    await page.waitForTimeout(7000) // Wait

    await page.goto(emailUrl);
    await page.waitForTimeout(7000) // Wait
    await page.getByRole('cell', { name: 'Appointment Request for Physiotherapy with pt Health' }).first().click();
    await expect(page.frameLocator('iframe[name="html_msg_body"]').getByText('We’ve received your online')).toBeVisible();
  });
});
