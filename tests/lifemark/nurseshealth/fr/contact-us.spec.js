import { test, expect } from "@playwright/test";
import dotenv from "dotenv";

dotenv.config();

const url = 'https://www.nurseshealth.ca/fr/nous-joindre'
const emailUrl = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${process.env.MAILNATOR_ID}`

test.use({ storageState: "./auth/auth-nurseshealth.json" })
test.describe('Nurseshealth Contact Us (French)', { tag: '@nurseshealth' }, () => {
  test('Contact Us french form should work', async ({ page }) => {
    await page.goto(url, { waitUntil: "domcontentloaded" });

    await page.check('#edit-i-am-a-radios-employer');

    const formFields = [
      { selector: '#edit-first-name', value: process.env.TESTER_NAME },
      { selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
      { selector: '#edit-phone-number', value: process.env.TESTER_PHONE },
      { selector: '#edit-email', value: process.env.TESTER_EMAIL },
      { selector: '#edit-message', value: process.env.TESTER_MESSAGE }
    ];

    for (const { selector, value } of formFields) {
        await page.fill(selector, value);
    }

    await page.click('#edit-actions-submit')

    await expect(page.locator('text="Nous vous remercions de nous avoir contactés"')).toBeVisible({ timeout: 60000 });

    await page.goto(emailUrl, { waitUntil: "domcontentloaded" }); //  Solution!
    await page.waitForTimeout(5000) // Wait 
    await page.getByRole('cell', { name: 'Soumission du formulaire Web' }).first().click();
    await expect(page.frameLocator('iframe[name="html_msg_body"]').getByText('Les valeurs soumises sont:')).toBeVisible();
    await expect(page.frameLocator('iframe[name="html_msg_body"]').getByText(`${process.env.TESTER_NAME}`)).toBeVisible();
    await expect(page.frameLocator('iframe[name="html_msg_body"]').getByText(`${process.env.TESTER_LASTNAME}`)).toBeVisible();
    await expect(page.frameLocator('iframe[name="html_msg_body"]').getByText(`${process.env.TESTER_PHONE}`)).toBeVisible();
    await expect(page.frameLocator('iframe[name="html_msg_body"]').getByText(`${process.env.TESTER_EMAIL}`)).toBeVisible();
  });

  test('Should show handle error message: required input', async ({ browser }) => {
    console.log(`URL IN USE: ${url}`)
    const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
    const page = await context.newPage();
    await page.goto(url);
    await page.getByLabel('Courriel').fill('');
    await page.getByRole('button', { name: 'Contactez-nous', exact: true }).click();
    await expect(page.getByText('*Prénom field is required.')).toBeVisible();
    await expect(page.getByText('*Nom de famille field is')).toBeVisible();
    await expect(page.getByText('Veuillez entrer une adresse')).toBeVisible();
    await expect(page.getByText('I am options field is')).toBeVisible();
    await expect(page.getByText('Message (champ ouvert) field')).toBeVisible();
  });
});
