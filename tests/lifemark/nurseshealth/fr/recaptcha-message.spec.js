// NOTE: These tests use a fixed viewport 
import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
import { randomString } from '../../../../assets/helpers/randomString.js';

dotenv.config();

test.describe('Recaptcha Message in Contact/Document Upload its in the right position', { tag: '@nurseshealth' }, () => {

  test('Contact Us French Recaptcha form should work in the right position', async ({ browser }) => {
    const url = 'https://www.nurseshealth.ca/fr/nous-joindre';
    /* const url = 'http://www.upcore.branchmanager.nurseshealth.ca/contact-us' */
    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD },
      viewport: { width: 1280, height: 800 }
    });
    const page = await context.newPage();
    await page.goto(url, { waitUntil: "domcontentloaded" });

    await page.check('#edit-i-am-a-radios-employer');

    const formFields = [
      { selector: '#edit-first-name', value: process.env.TESTER_NAME },
      { selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
      { selector: '#edit-phone-number', value: process.env.TESTER_PHONE },
      { selector: '#edit-email', value: process.env.TESTER_EMAIL },
      { selector: '#edit-message', value: process.env.TESTER_MESSAGE }
    ];

    for (const { selector, value } of formFields) {
        await page.fill(selector, value);
    }

    page.click("#edit-actions-submit")

    await page.waitForTimeout(12000);

    const position = await page.evaluate(() => {
      const element = document.querySelector('div[role="alert"].alert-danger');
      const rect = element.getBoundingClientRect();
      return rect.top;
    });

    expect(position).toBeGreaterThanOrEqual(370);
    expect(position).toBeLessThanOrEqual(390);
  });

  test('Document Upload French Recaptcha form should work in the right position', async ({ browser }) => {
    const url = 'https://www.nurseshealth.ca/fr/téléversement-sécurisé-de-documents'
    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD },
      viewport: { width: 1280, height: 800 }
    });
    const page = await context.newPage();

    await page.goto(url);

    const formFields = [
        { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
        { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
        { label: 'Title', selector: '#edit-title', value: process.env.TESTER_SUBJECT },
        { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
        { label: 'Message', selector: '#edit-comments', value: `${process.env.TESTER_MESSAGE} ${randomString()}` }
    ];

    for (const { selector, value } of formFields) {
        await page.fill(selector, value);
    }

    await page.getByText('Sélectionner les fichiers').click();
    await page.getByLabel('Sélectionner les fichiers').setInputFiles('assets/files/example_text.txt');
    await page.waitForTimeout(5000) // Wait for upload files

    await page.getByText('Soumettre').click();
    await page.waitForTimeout(15000)

    const position = await page.evaluate(() => {
      const element = document.querySelector('div[role="alert"].alert-danger');
      const rect = element.getBoundingClientRect();
      return rect.top;
    });

    expect(position).toBeGreaterThanOrEqual(470);
    expect(position).toBeLessThanOrEqual(490);
  });

});
