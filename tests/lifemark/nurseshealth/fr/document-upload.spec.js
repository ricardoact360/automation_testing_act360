import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
import { randomString } from '../../../../assets/helpers/randomString.js';

dotenv.config();

const url = 'https://www.nurseshealth.ca/fr/téléversement-sécurisé-de-documents'
const emailUrl = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${process.env.MAILNATOR_ID}`

test.use({ storageState: "./auth/auth-nurseshealth.json" })

test.describe('Nurseshealth Document Upload (French)', { tag: '@nurseshealth' }, () => {
  test('Document upload french form. It should be possible to upload and send a document', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD } });
      const page = await context.newPage();

      await page.goto(url);

      const formFields = [
          { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
          { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Title', selector: '#edit-title', value: process.env.TESTER_SUBJECT },
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'Message', selector: '#edit-comments', value: `${process.env.TESTER_MESSAGE} ${randomString()}` }
      ];

      // Fill fields.
      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      await page.getByText('Sélectionner les fichiers').click();
      await page.getByLabel('Sélectionner les fichiers').setInputFiles('assets/files/example_text.txt');
      await page.waitForTimeout(7000) // Wait for upload files
      await expect(page.getByText(/example_text(_\d+)?\.txt/).first()).toBeVisible();
      
      await page.getByText('Soumettre').click();
      await expect(page.locator('text="Merci pour votre soumission!"')).toBeVisible();

      await page.goto(emailUrl);
      await page.waitForTimeout(7000)
      await page.getByRole('cell', { name: 'Webform submission from: Téléversement sécurisé de documents' }).first().click();
      await expect(page.frameLocator('iframe[name="html_msg_body"]').getByRole('heading', { name: 'Merci d’avoir téléversé vos documents de façon sécuritaire' })).toBeVisible();
  });

  test('The form must have all the inputs empty and must be able to fill them.', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
      const page = await context.newPage();

      await page.goto(url);

      const formFields = [
          { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
          { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Title', selector: '#edit-title', value: process.env.TESTER_SUBJECT },
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'Message', selector: '#edit-comments', value: process.env.TESTER_MESSAGE }
      ];

      // Check empty inputs.
      for (const { _label, selector } of formFields) {
          await page.waitForSelector(selector, { state: 'visible' });
          const locator = page.locator(selector);
          await expect(locator).toBeEmpty({ timeout: 10000 });
      }

      // Fill fields.
      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      // Verify that the fields have been filled in.
      for (const { selector, value } of formFields) {
          const locator = page.locator(selector);
          await expect(locator).toHaveValue(value, { timeout: 10000 });
      }   
  });

  test('Should show an error if try to submit with no file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
      const page = await context.newPage();

      await page.goto(url);

      const formFields = [
          { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
          { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Title', selector: '#edit-title', value: process.env.TESTER_SUBJECT },
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'Message', selector: '#edit-comments', value: process.env.TESTER_MESSAGE }
      ];

      // Fill fields.
      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }
      
      await page.getByText('Soumettre').click();
      await expect(page.getByText('Ajouter un nouveau fichier est requis')).toBeVisible();
  });


  test('Should show error if try to upload a JS file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
      const page = await context.newPage();

      await page.goto(url);

      await page.getByText('Sélectionner les fichiers').click();
      await page.getByLabel('Sélectionner les fichiers').setInputFiles('assets/files/example_js.js');
      await expect(page.locator('li', { hasText: 'Only files with the following extensions are allowed:' }).first()).toBeVisible();
  });

  test('Should show error if try to upload a PHP file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
      const page = await context.newPage();

      await page.goto(url);

      await page.getByText('Sélectionner les fichiers').click();
      await page.getByLabel('Sélectionner les fichiers').setInputFiles('assets/files/example_php.php');
      await expect(page.locator('li', { hasText: 'Only files with the following extensions are allowed:' }).first()).toBeVisible();
  });

  test('Should show error if try to upload a JSP file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
      const page = await context.newPage();

      await page.goto(url);

      await page.getByText('Sélectionner les fichiers').click();
      await page.getByLabel('Sélectionner les fichiers').setInputFiles('assets/files/example_jsp.jsp');
      await expect(page.locator('li', { hasText: 'Only files with the following extensions are allowed:' }).first()).toBeVisible();
  });

  test('Should it able to remove uploaded file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
      const page = await context.newPage();

      await page.goto(url);

      await page.getByText('Sélectionner les fichiers').click();
      await page.getByLabel('Sélectionner les fichiers').setInputFiles('assets/files/example_text.txt');
      await page.waitForTimeout(7000) // Wait for upload files
      await expect(page.getByText(/example_text(_\d+)?\.txt/).first()).toBeVisible();
      await page.locator('[id*="edit-add-a-new-file--"] span').nth(2).check();
      await page.getByRole('button', { name: 'Retirer des fichiers de la sélection' }).click();
      await expect(page.getByText(/example_text(_\d+)?\.txt/).first()).not.toBeVisible();
  });

  test('Should show handle error message: required input', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
      const page = await context.newPage();
      await page.goto(url);
      await page.getByRole('button', { name: 'Soumettre' }).click();
      await expect(page.getByText('Le prénom est requis.')).toBeVisible();
      await expect(page.getByText('Le nom est requis.')).toBeVisible();
      await expect(page.getByText('L’adresse de courriel est')).toBeVisible();

  });
});
