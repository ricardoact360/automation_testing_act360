import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
import { randomString } from '../../../../assets/helpers/randomString.js';

dotenv.config();

const url = 'https://www.nurseshealth.ca/document-upload'
const emailUrl = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${process.env.MAILNATOR_ID}`

test.use({ storageState: "./auth/auth-nurseshealth.json" })

test.describe('Nurseshealth Document Upload (English)', { tag: '@nurseshealth' }, () => {
  test('Document upload english form should work', async ({ page }) => {
      await page.goto(url);

      const formFields = [
          { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
          { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Title', selector: '#edit-title', value: process.env.TESTER_SUBJECT },
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'Message', selector: '#edit-comments', value: `${process.env.TESTER_MESSAGE} ${randomString()}` }
      ];

      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      await page.getByText('Choose Files').click();
      await page.getByLabel('Choose Files').setInputFiles('assets/files/example_text.txt');
      await page.waitForTimeout(7000) // Wait for upload files
      await expect(page.getByText(/example_text(_\d+)?\.txt/).first()).toBeVisible();

      await page.click('#edit-submit');
      await expect(page.locator('text="Thank you for your submission!"')).toBeVisible();

      await page.goto(emailUrl);
      await page.waitForTimeout(7000)
      await page.getByRole('cell', { name: 'Documents Submitted : Secure Document Uploader' }).first().click();
      await expect(page.frameLocator('iframe[name="html_msg_body"]').getByRole('heading', { name: 'Thank you for uploading your Secure Documents' })).toBeVisible();
  });

  test('Should show an error if try to submit with no file', async ({ page }) => {
      await page.goto(url);

      const formFields = [
          { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
          { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Title', selector: '#edit-title', value: process.env.TESTER_SUBJECT },
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'Message', selector: '#edit-comments', value: process.env.TESTER_MESSAGE }
      ];

      // Fill fields.
      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }
      
      await page.click('#edit-submit');
      await expect(page.getByText('A file was not attached')).toBeVisible();
  });

  test('Should show error if try to upload a JS file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
      const page = await context.newPage();

      await page.goto(url);

      await page.getByText('Choose Files').click();
      await page.getByLabel('Choose Files').setInputFiles('assets/files/example_js.js');
      await expect(page.locator('li', { hasText: 'Only files with the following extensions are allowed:' }).first()).toBeVisible();
  });

  test('Should show error if try to upload a PHP file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
      const page = await context.newPage();

      await page.goto(url);

      await page.getByText('Choose Files').click();
      await page.getByLabel('Choose Files').setInputFiles('assets/files/example_php.php');
      await expect(page.locator('li', { hasText: 'Only files with the following extensions are allowed:' }).first()).toBeVisible();
  });

  test('Should show error if try to upload a JSP file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)
      const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
      const page = await context.newPage();

      await page.goto(url);

      await page.getByText('Choose Files').click();
      await page.getByLabel('Choose Files').setInputFiles('assets/files/example_jsp.jsp');
      await expect(page.locator('li', { hasText: 'Only files with the following extensions are allowed:' }).first()).toBeVisible();

  });

  test('Should be able to remove uploaded file', async ({ browser }) => {
    const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
    const page = await context.newPage();

    await page.goto(url);
    await page.getByText('Choose Files').click();
    await page.getByLabel('Choose Files').setInputFiles('assets/files/example_text.txt');
    await page.waitForTimeout(7000);

    async function checkFilePresence(fileName) {
      try {
        await expect(page.getByText(fileName).first()).toBeVisible({ timeout: 5000 });
        return true;
      } catch {
        return false;
      }
    }

    const fileNames = ['example_text.txt', /example_text_\d+\.txt/];
    let foundFileName;

    for (const fileName of fileNames) {
      if (await checkFilePresence(fileName)) {
        foundFileName = fileName;
        break;
      }
    }

    if (!foundFileName) {
      throw new Error('No file with the expected name was found');
    }

    await page.locator('[id*="edit-add-a-new-file--"] span').nth(2).check();
    await page.getByRole('button', { name: 'Remove selected' }).click();

    await expect(page.getByText(foundFileName).first()).not.toBeVisible({ timeout: 10000 });
  });

  test('Should show handle error message: required input', async ({ browser }) => {
    console.log(`URL IN USE: ${url}`)
    const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
    const page = await context.newPage();
    await page.goto(url);
    await page.getByLabel('Email').fill('');
    await page.getByRole('button', { name: 'Submit' }).click();
    await expect(page.getByText('First Name is required.')).toBeVisible();
    await expect(page.getByText('Last Name is required.')).toBeVisible();
    await expect(page.getByText('Email is required.')).toBeVisible();
  });
});
