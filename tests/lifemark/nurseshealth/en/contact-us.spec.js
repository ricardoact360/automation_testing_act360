import { test, expect } from "@playwright/test";
import dotenv from "dotenv";

dotenv.config();

const url = 'https://www.nurseshealth.ca/contact-us';
/* const url = 'http://www.upcore.branchmanager.nurseshealth.ca/contact-us' */
const emailUrl = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${process.env.MAILNATOR_ID}`;

test.use({ storageState: "./auth/auth-nurseshealth.json" })
test.describe('Nurseshealth Contact Us (English)', { tag: '@nurseshealth' }, () => {
  test('Contact Us english form should work', async ({ browser }) => {
    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });
    const page = await context.newPage();
    await page.goto(url, { waitUntil: "domcontentloaded" });

    await page.check('#edit-i-am-a-radios-employer');

    const formFields = [
      { selector: '#edit-first-name', value: process.env.TESTER_NAME },
      { selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
      { selector: '#edit-phone-number', value: process.env.TESTER_PHONE },
      { selector: '#edit-email', value: process.env.TESTER_EMAIL },
      { selector: '#edit-message', value: process.env.TESTER_MESSAGE }
    ];

    for (const { selector, value } of formFields) {
        await page.fill(selector, value);
    }

    page.click("#edit-actions-submit")

    await page.waitForTimeout(10000);

    await expect(page.locator('text="Thank you for contacting us"')).toBeVisible({ timeout: 60000 });

    await page.goto(emailUrl, { waitUntil: "domcontentloaded" }); //  Solution!
    await page.waitForTimeout(5000) // Wait 
    await page.getByRole('cell', { name: 'Webform submission from:' }).first().click();
    await expect(page.frameLocator('iframe[name="html_msg_body"]').getByRole('heading', { name: 'Thank you for contacting us' })).toBeVisible();
  });

  test('Should show handle error message: required input', async ({ browser }) => {
    console.log(`URL IN USE: ${url}`)
    const context = await browser.newContext({ httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD } });
    const page = await context.newPage();
    await page.goto(url);
    await page.getByRole('button', { name: 'Submit' }).click();
    await expect(page.getByText('*First Name field is required.')).toBeVisible();
    await expect(page.getByText('*Last Name field is required.')).toBeVisible();
    await page.getByText('Message field is required.').click();
    await page.getByText('I am options field is').click();
  });

  test('Should toll div and secure document div has the same right position', async ({ browser }) => {
    console.log(`URL IN USE: ${url}`)
    const context = await browser.newContext({ httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD } });
    const page = await context.newPage();
    await page.goto(url);

    const tollFreeInCanadaDiv = page.locator('.col-md-12.col-lg-12.gradient-2.right');
    const secureDiv = page.locator('.col-md-5.col-lg-5.gradient-2.secure-doc-button');

    const tollDiv = await tollFreeInCanadaDiv.evaluate(el => el.getBoundingClientRect());
    const secureRect = await secureDiv.evaluate(el => el.getBoundingClientRect());

    expect(tollDiv.right).toBeCloseTo(secureRect.right, 0);
  });
});
