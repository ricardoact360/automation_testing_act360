import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://lifeisnow.ca/contact/'

test.describe('LifeIsNow Contact Us', { tag: '@lifeisnow' }, () => {
    test.skip('Contact Us form should work', async ({ page }) => {
        await page.goto(url);

        const formFields = [
            { label: 'Name', selector: 'input[name="your-name"]', value: process.env.TESTER_FULLNAME},
            { label: 'Email Address', selector: 'input[name="your-email"]', value: process.env.TESTER_EMAIL },
            { label: 'Phone number', selector: 'input[name="phone-number"]', value: process.env.TESTER_PHONE },
            { label: 'Message', selector: 'textarea[name="message"]', value: process.env.TESTER_MESSAGE }
        ];

        // Check empty inputs.
        for (const { _label, selector } of formFields) {
            await page.waitForSelector(selector, { state: 'visible' });
            const locator = page.locator(selector);
            await expect(locator).toBeEmpty({ timeout: 10000 });
        }

        // Fill fields.
        for (const { selector, value } of formFields) {
            await page.fill(selector, value);
        }

        // Verify that the fields have been filled in.
        for (const { selector, value } of formFields) {
            const locator = page.locator(selector);
            await expect(locator).toHaveValue(value, { timeout: 10000 });
        }

        await page.click('input[type="submit"].wpcf7-form-control.wpcf7-submit.has-spinner.button');

        await expect(page.locator('text="Your message was sent successfully, thanks!"')).toBeVisible();
    });

    test('Should clean form when press Clear button', async ({ page }) => {
        await page.goto('https://lifeisnow.ca/contact/');

        const formFields = [
            { label: 'Name', selector: 'input[name="your-name"]', value: process.env.TESTER_NAME},
            { label: 'Email Address', selector: 'input[name="your-email"]', value: process.env.TESTER_EMAIL },
            { label: 'Phone number', selector: 'input[name="phone-number"]', value: process.env.TESTER_PHONE },
            { label: 'Message', selector: 'textarea[name="message"]', value: process.env.TESTER_MESSAGE }
        ];

        // Check empty inputs.
        for (const { _label, selector } of formFields) {
            await page.waitForSelector(selector, { state: 'visible' });
            const locator = page.locator(selector);
            await expect(locator).toBeEmpty({ timeout: 10000 });
        }

        // Fill fields.
        for (const { selector, value } of formFields) {
            await page.fill(selector, value);
        }

        // Verify that the fields have been filled in.
        for (const { selector, value } of formFields) {
            const locator = page.locator(selector);
            await expect(locator).toHaveValue(value, { timeout: 10000 });
        }

        await page.click('input[type="reset"].clearButton');

        // Check empty inputs.
        for (const { _label, selector } of formFields) {
            await page.waitForSelector(selector, { state: 'visible' });
            const locator = page.locator(selector);
            await expect(locator).toBeEmpty({ timeout: 10000 });
        }    
    });

    test('Should should show an error if all inputs are not filled', async ({ page }) => {
        await page.goto('https://lifeisnow.ca/contact/');

        const formFields = [
            { label: 'Name', selector: 'input[name="your-name"]', value: process.env.TESTER_NAME},
            { label: 'Phone number', selector: 'input[name="phone-number"]', value: process.env.TESTER_PHONE },
            { label: 'Message', selector: 'textarea[name="message"]', value: process.env.TESTER_MESSAGE }
        ];

        // Check empty inputs.
        for (const { _label, selector } of formFields) {
            await page.waitForSelector(selector, { state: 'visible' });
            const locator = page.locator(selector);
            await expect(locator).toBeEmpty({ timeout: 10000 });
        }

        // Fill fields.
        for (const { selector, value } of formFields) {
            await page.fill(selector, value);
        }

        // Verify that the fields have been filled in.
        for (const { selector, value } of formFields) {
            const locator = page.locator(selector);
            await expect(locator).toHaveValue(value, { timeout: 10000 });
        }

        await page.click('input[type="submit"].wpcf7-form-control.wpcf7-submit.has-spinner.button');

        await expect(page.locator('div.wpcf7-response-output').first()).toBeVisible();

    });
});
