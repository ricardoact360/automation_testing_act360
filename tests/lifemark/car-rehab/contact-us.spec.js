import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://car-rehab.com/contact/'

test.describe('car-rehab Contact Us', { tag: '@car-rehab' }, () => {
  test('Contact Us form should work', async ({ page }) => {
      await page.goto(url);

      const formFields = [
          { label: 'First name', selector: '#wpforms-219-field_0', value: process.env.TESTER_NAME},
          { label: 'Last name', selector: '#wpforms-219-field_0-last', value: process.env.TESTER_LASTNAME},
          { label: 'Phone number', selector: '#wpforms-219-field_3', value: process.env.TESTER_PHONE },
          { label: 'Email Address', selector: '#wpforms-219-field_1', value: process.env.TESTER_EMAIL },
          { label: 'Message', selector: '#wpforms-219-field_2', value: process.env.TESTER_MESSAGE }
      ];

      // Check empty inputs.
      for (const { _label, selector } of formFields) {
          await page.waitForSelector(selector, { state: 'visible' });
          const locator = page.locator(selector);
          await expect(locator).toBeEmpty({ timeout: 10000 });
      }

      // Fill fields.
      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      // Verify that the fields have been filled in.
      for (const { selector, value } of formFields) {
          const locator = page.locator(selector);
          await expect(locator).toHaveValue(value, { timeout: 10000 });
      }

      await page.getByText('Submit').click();

      await expect(page.locator('text="Thanks for contacting us! We will be in touch with you shortly."')).toBeVisible();
  });
});
