import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();
import { randomString } from "../../../assets/helpers/randomString"

const url = 'https://vp-group.ca/contact-us/referral-form'
// const url = 'https://www.upcore.branchmanager.vp-group.ca/contact-us/referral-form'
// const url = 'https://uat.vp-group.ca/referral-form'

test.describe('Vp-Group Referral Form', { tag: '@vp-group' }, () => {
  test('Referral Form form should work with Newfoundland', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)

      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });
      const page = await context.newPage();
      await page.goto(url);

      await page.getByText('Close').click();

      const formFieldsStepOne = [
          { label: 'Company', selector: '#edit-company-name', value: process.env.TESTER_COMPANY },
          { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
          { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Role/Title', selector: '#edit-role-title', value: 'QA' },
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'City', selector: '#edit-city', value: process.env.TESTER_CITY },
          { label: 'Phone number', selector: '#edit-phone-number', value: process.env.TESTER_PHONE },
          { label: 'FAX number', selector: '#edit-fax-number', value: process.env.TESTER_FAX_NUMBER },
          { label: 'Address', selector: '#edit-address', value: 'Canada' },
          { label: 'City', selector: '#edit-city', value: 'Toronto' },
          { label: 'Edit Postal Code', selector: '#edit-postal-code', value: 'Toronto' },
      ];

      // Select Province
      await page.selectOption('#edit-province', 'Newfoundland and Labrador');

      for (const { selector, value } of formFieldsStepOne) {
          await page.fill(selector, value);
      }

      // >>>>>>>>>>>>>>>>>> NEXT >>>>>>>>>>>>>>>>>
      await page.getByRole('button', { name: 'Next' }).click();

      await page.getByLabel('First name').fill('Test');
      await page.getByLabel('Last name', { exact: true }).fill('Lord');

      /* await page.getByLabel('Salutation').selectOption('Miss'); */

      /* await page.waitForTimeout(3000) // Wait  */
      /* await page.getByLabel('Gender').selectOption('Male'); */
      await page.getByLabel('Date of birth').fill('1990-12-09');
      await page.getByRole('textbox', { name: 'Email' }).fill('test_qa@act360.ca');
      await page.getByRole('textbox', { name: 'Phone number', exact: true }).fill('2312322334');
      await page.getByLabel('Permission to send text').selectOption('Yes');
      await page.getByRole('textbox', { name: 'Address' }).fill('Canada');
      await page.getByRole('textbox', { name: 'City' }).fill('Montreal');
      await page.selectOption('select[name="examinee_province"]', 'nb');
      await page.getByLabel('Date of disability/issue').fill('2012-12-08');
      await page.getByLabel('Claim/identifying number').fill('99999');
      await page.getByLabel('Permission for Viewpoint to').selectOption('Yes');
      await page.getByLabel('Special contact instructions?').fill('No');
      await page.getByLabel('Interpretation required?').selectOption('no');
      // await page.locator('#edit-examinee-information--YuO7DJm9b9E').click();
      await page.getByLabel('Transportation required?').selectOption('No');
      await page.getByLabel('Is there a legal').selectOption('No');

      // >>>>>>>>>>>>>>>>>> NEXT >>>>>>>>>>>>>>>>>
      await page.getByRole('button', { name: 'Next' }).click();

      const formFieldsStepThree = [
          { label: 'Service(s) requested?', selector: 'textarea[id^="edit-service-s-requested---"]', value: 'No' },
          { label: 'Special instructions for this referral?', selector: 'input[id^="edit-special-instructions-for-this-referral---"]', value: 'No' },
          { label: 'Report should be delivered to?', selector: 'input[id^="edit-report-should-be-delivered-to---"]', value: process.env.TESTER_FULLNAME },
          { label: 'Invoice to?', selector: 'input[id^="edit-invoice-to--"]', value: process.env.TESTER_FULLNAME },
      ];

      // Fill fields.
      for (const { selector, value } of formFieldsStepThree) {
          await page.fill(selector, value);
      }

      await page.getByText('Choose Files').click();
      await page.getByLabel('Choose Files').setInputFiles('assets/files/example_text.txt');
      await expect(page.getByText(/example_text.*\.txt/)).toBeVisible();

      // Final Submit!
      /* await page.getByText('Submit').click(); */

      /* await expect(page.locator('text="Thank you for your submission!"')).toBeVisible({ timeout: 10000 }); */
  });

  test.skip('Referral Form form should work with Alberta', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)

      const context = await browser.newContext({
          httpCredentials: {
              username: process.env.LIFEMARK_USERNAME,
              password: process.env.LIFEMARK_PASSWORD
          }
      });
      const page = await context.newPage();

      await page.goto(url);

      await page.getByText('Close').click();

      const formFieldsStepOne = [
          { label: 'Company', selector: '#edit-company-name', value: process.env.TESTER_COMPANY },
          { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
          { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Role/Title', selector: '#edit-role-title', value: 'QA' },
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'City', selector: '#edit-city', value: process.env.TESTER_CITY },
          { label: 'Phone number', selector: '#edit-phone-number', value: process.env.TESTER_PHONE },
          { label: 'FAX number', selector: '#edit-fax-number', value: process.env.TESTER_FAX_NUMBER },
          { label: 'Address', selector: '#edit-address', value: 'Canada' },
          { label: 'City', selector: '#edit-city', value: 'Toronto' },
          { label: 'Edit Postal Code', selector: '#edit-postal-code', value: 'Toronto' },
      ];

      // Select Province
      await page.selectOption('#edit-province', 'Alberta');

      // Fill fields.
      for (const { selector, value } of formFieldsStepOne) {
          await page.fill(selector, value);
      }

      await page.click('#edit-wizard-next');

      await expect(page.locator('text="Examinee information:"')).toBeVisible({ timeout: 7000 });

      const formFieldsStepTwo = [
          { label: 'First Name', selector: 'input[id^="edit-examinee-first-name--"]', value: 'Test' },
          { label: 'Last Name', selector: 'input[id^="edit-examinee-last-name--"]', value: 'Lord' },
          { label: 'Date of birth', selector: 'input[id^="edit-examinee-date-of-birth--"]', value: '1990-09-14' },
          { label: 'Email', selector: 'input[id^="edit-examinee-email--"]', value: 'test@act360.ca' },
          { label: 'Address', selector: 'input[id^="edit-examinee-address--"]', value: 'Canada' },
          { label: 'City', selector: 'input[id^="edit-examinee-city--"]', value: 'Toronto' },
          { label: 'Edit Postal Code', selector: 'input[id^="edit-examinee-postal-code--"]', value: 'A0A - A1Y' },
          { label: 'Date of disability/issue', selector: 'input[id^="edit-date-of-disability-issue--"]', value: '1990-09-14' },
          { label: 'Claim/identifying number', selector: 'input[id^="edit-claim-identifying-number--"]', value: `qa_${randomString()}` },
          { label: 'Special contact instructions?', selector: 'input[id^="edit-special-contact-instructions---"]', value: 'no' },
      ];

      // Fill fields.
      for (const { selector, value } of formFieldsStepTwo) {
          await page.fill(selector, value);
      }

      await page.waitForTimeout(3000) // Wait 
      await page.selectOption('select[id^="edit-salutation--"]', 'Mr'); // Salutation
      await page.selectOption('select[id^="edit-gender--"]', 'Male'); // Gender
      await page.selectOption('select[id^="edit-permission-to-send-text-appointment-reminders--"]', 'No'); // Permission to send text appointment reminders
      await page.fill('input[id^="edit-examinee-address--"]', 'Address'); // Address
      await page.selectOption('select[id^="edit-examinee-province--"]', 'nl'); // Province
      await page.selectOption('select[id^="edit-permission-for-viewpoint-to-email-text-examinee-directly-for-app--"]', 'No'); // Permission for Viewpoint to email/text examinee directly for appointment reminders?
      await page.selectOption('select[id^="edit-interpretation-required--"]', 'no'); // Interpretation required?
      await page.selectOption('select[id^="edit-transportation-required--"]', 'No'); // Transportation required?
      await page.selectOption('select[id^="edit-is-there-a-legal-representative--"]', 'No'); // Is there a legal representative?
      // await page.selectOption('#', ''); 
      
      await page.getByText('Next').click();
      
      await expect(page.locator('text="Please let us know what we can arrange for you:"')).toBeVisible({ timeout: 7000 });

      const formFieldsStepThree = [
          { label: 'Service(s) requested?', selector: 'textarea[id^="edit-service-s-requested---"]', value: 'No' },
          { label: 'Special instructions for this referral?', selector: 'input[id^="edit-special-instructions-for-this-referral---"]', value: 'No' },
          { label: 'Report should be delivered to?', selector: 'input[id^="edit-report-should-be-delivered-to---"]', value: process.env.TESTER_FULLNAME },
          { label: 'Invoice to?', selector: 'input[id^="edit-invoice-to--"]', value: process.env.TESTER_FULLNAME },
      ];

      // Fill fields.
      for (const { selector, value } of formFieldsStepThree) {
          await page.fill(selector, value);
      }

      await page.getByText('Choose Files').click();
      await page.getByLabel('Choose Files').setInputFiles(['assets/files/example_text.txt', 'assets/files/example_text_two.txt']);
      await expect(page.getByText(/^example_text_\d+\.txt$/)).toBeVisible();
      await expect(page.getByText(/^example_text_two_\d+\.txt$/)).toBeVisible();

      // Final Submit!
      await page.getByText('Submit').click();

      await expect(page.locator('text="Thank you for your submission!"')).toBeVisible({ timeout: 10000 });
  });
});
