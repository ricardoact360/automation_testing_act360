import { test, expect } from "@playwright/test";

const url = 'https://vp-group.ca/'

test.use({
  viewport: { height: 800, width: 360 }
});

test.describe('Vp-Group Homepage', { tag: '@vp-group' }, () => {
  test('Accordion should work', async ({ page }) => {
    await page.goto(url);
    await page.getByRole('heading', { name: 'Customized Solutions.' }).click();
    await expect(page.getByText('We tailor services and build')).toBeVisible();
    await page.getByRole('heading', { name: 'We’re in This Together.' }).click();
    await expect(page.getByText('We have the experience to')).toBeVisible();
    await page.getByRole('heading', { name: 'Foundation of Experts.' }).click();
    await expect(page.getByText('Our organization was')).toBeVisible();
    await page.getByRole('heading', { name: 'Real Results.' }).click();
    await expect(page.getByText('We deliver quality, unbiased')).toBeVisible();
  });
});


