import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://www.vp-group.ca/careers/medical-experts-consultants'
// const url = 'https://www.upcore.branchmanager.vp-group.ca/contact-us/secure-document-upload'

test.use({ storageState: "./auth/auth-vp-group.json" })
test.describe('Vp-Group Medical Experts Consultants', { tag: '@vp-group' }, () => {
  test('Medical Experts Consultants form should work', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)

      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });

      const page = await context.newPage();

      await page.goto(url);

      const formFieldsStepOne = [
          { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
          { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'Specialty', selector: '#edit-specialty', value: process.env.TESTER_SUBJECT },
          { label: 'Phone', selector: '#edit-phone', value: process.env.TESTER_PHONE},
      ];

      // Select Province
      await page.selectOption('#edit-province', 'nl');

      // Fill fields.
      for (const { selector, value } of formFieldsStepOne) {
          await page.fill(selector, value);
      }

      await page.click("#edit-submit")
      await page.waitForTimeout(10000)
      await expect(page.locator('text="New submission added to Join our team."')).toBeVisible({ timeout: 7000 });

  });

});

