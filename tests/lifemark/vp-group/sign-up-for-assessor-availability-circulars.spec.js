import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://vp-group.ca/contact-us/sign-up-for-assessor-availability-circulars'
// const url = 'https://www.upcore.branchmanager.vp-group.ca/contact-us/sign-up-for-assessor-availability-circulars'

test.use({ storageState: "./auth/auth-vp-group.json" })
test.describe('Vp-Group Sign Up for Regular Updates on Assessor Availability', { tag: '@vp-group' }, () => {

  const formFields = [
      { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
      { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
      { label: 'Company', selector: '#edit-company-name', value: process.env.TESTER_COMPANY },
      { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
      { label: 'City', selector: '#edit-city', value: process.env.TESTER_CITY }
  ];

  test('Sign Up for Regular Updates on Assessor Availability form should work', async ({ browser }) => {
      console.log(`URL in use is: ${url}`)

      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });
      const page = await context.newPage();

      await page.goto(url);

      // Select Province
      await page.selectOption('#edit-province', 'nt');

      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      await page.click('#edit-submit');

      await page.waitForTimeout(10000) // Wait

      await expect(page.locator('text="Thank you for your submission!"')).toBeVisible();
  });

  test('Form is empty by default and should be able to fill all inputs correctly', async ({ browser }) => {
      console.log(`URL in use is: ${url}`)

      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });
      const page = await context.newPage();

      await page.goto(url);

      // Select Province
      await page.selectOption('#edit-province', 'nt');

      // Check empty inputs.
      for (const { _label, selector } of formFields) {
          await page.waitForSelector(selector, { state: 'visible' });
          const locator = page.locator(selector);
          await expect(locator).toBeEmpty({ timeout: 10000 });
      }

      // Fill fields.
      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      // Verify that the fields have been filled in.
      for (const { selector, value } of formFields) {
          const locator = page.locator(selector);
          await expect(locator).toHaveValue(value, { timeout: 10000 });
      }
  });
});
