import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://www.vp-group.ca/contact-us'
/* const url = 'https://dev.branchmanager.vp-group.ca/contact-us' */
// const url = 'https://www.upcore.branchmanager.vp-group.ca/contact-us'

test.use({ storageState: "./auth/auth-vp-group.json" })
test.describe('Vp-Group Contact Us', { tag: '@vp-group' }, () => {
  test('Contact Us form should work', async ({ browser }) => {
    const context = await browser.newContext({
        httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
    });
    const page = await context.newPage();
    await page.goto(url);

    const formFields = [
        { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
        { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
        { label: 'Phone number', selector: '#edit-phone-number', value: process.env.TESTER_PHONE },
        { label: 'Company', selector: '#edit-company', value: process.env.TESTER_COMPANY },
        { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
        { label: 'Message', selector: '#edit-comments', value: process.env.TESTER_MESSAGE }
    ];

    await page.selectOption('#edit-province', 'on');

    for (const { selector, value } of formFields) {
        await page.fill(selector, value);
    }

    await page.click("#edit-submit")

    await page.waitForTimeout(10000)

    await expect(page.locator('text="Thank you for your submission!"')).toBeVisible();
  });

  test('Should show error when enter a non North America phone number', async ({ browser }) => {
      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });
      const page = await context.newPage();

      await page.goto(url);

      const formFields = [
          { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
          { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Phone number', selector: '#edit-phone-number', value: process.env.TESTER_NON_AMERICAN_PHONE }, 
          { label: 'Company', selector: '#edit-company', value: process.env.TESTER_COMPANY },
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'Message', selector: '#edit-comments', value: '-- PLEASE, IGNORE -- This is a test.' }
      ];

      // Select Province
      await page.selectOption('#edit-province', 'on');

      // Check empty inputs.
      for (const { _label, selector } of formFields) {
          await page.waitForSelector(selector, { state: 'visible' });
          const locator = page.locator(selector);
          await expect(locator).toBeEmpty({ timeout: 10000 });
      }

      // Fill fields.
      for (const { selector, value } of formFields) {
          await page.fill(selector, value);
      }

      // Verify that the fields have been filled in.
      for (const { selector, value } of formFields) {
          const locator = page.locator(selector);
          await expect(locator).toHaveValue(value, { timeout: 10000 });
      }

      await page.click('#edit-submit');

      await expect(page.locator('text="Please enter a valid North American phone number."')).toBeVisible();
  });
});
