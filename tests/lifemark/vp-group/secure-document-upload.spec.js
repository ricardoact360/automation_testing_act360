import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://vp-group.ca/contact-us/secure-document-upload'
// const url = 'https://uat.vp-group.ca/secure-document-upload'

test.use({ storageState: "./auth/auth-vp-group.json" })
test.describe('Vp-Group Secure Document Upload', { tag: '@vp-group' }, () => {
  test('Secure document upload form should work', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)

      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });

      const page = await context.newPage();

      await page.goto(url);

      const formFieldsStepOne = [
          { label: 'First Name', selector: '#edit-first-name', value: process.env.TESTER_NAME },
          { label: 'Last Name', selector: '#edit-last-name', value: process.env.TESTER_LASTNAME },
          { label: 'Email', selector: '#edit-email', value: process.env.TESTER_EMAIL },
          { label: 'Company', selector: '#edit-company-name', value: process.env.TESTER_COMPANY },
          { label: 'Examine First Name', selector: '#edit-examinee-first-name', value: 'Javier' },
          { label: 'Examine Last Name', selector: '#edit-examinee-last-name', value: 'Moreno' },
          { label: 'Claim/File Reference Number', selector: '#edit-claim-file-reference-number', value: '2222' },
          { label: 'Comments', selector: '#edit-comments', value: process.env.TESTER_MESSAGE },
      ];

      // Select Province
      await page.selectOption('#edit-province', 'nl');

      // Fill fields.
      for (const { selector, value } of formFieldsStepOne) {
          await page.fill(selector, value);
      }

      await page.setInputFiles('input[type="file"]', 'assets/files/example_text.txt');
      await page.waitForTimeout(3000) // Wait 
      await page.click('text=Start Upload');
      await expect(page.locator('#edit-plupload-field_container')).toContainText('100%');

      await page.click("#edit-submit")
      await page.waitForTimeout(10000)
      await expect(page.locator('text="Thank you for your submission!"')).toBeVisible({ timeout: 9000 });
  });

  test('Should show error if try to upload a JS file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)

      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });

      const page = await context.newPage();

      await page.goto(url);

      page.on('dialog', async (dialog) => {
        expect(dialog.type()).toContain('alert');
        expect(dialog.message()).toContain('Error: Invalid file extension: example_js');
        await dialog.accept();
      });

      await page.setInputFiles('input[type="file"]', 'assets/files/example_js.js');
  });

  test('Should show error if try to upload a PHP file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)

      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });

      const page = await context.newPage();

      await page.goto(url);

      page.on('dialog', async (dialog) => {
        expect(dialog.type()).toContain('alert');
        expect(dialog.message()).toContain('Error: Invalid file extension: example_php');
        await dialog.accept();
      });

      await page.setInputFiles('input[type="file"]', 'assets/files/example_php.php');
  });

  test('Should show error if try to upload a JSP file', async ({ browser }) => {
      console.log(`URL IN USE: ${url}`)

      const context = await browser.newContext({
          httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }
      });

      const page = await context.newPage();

      await page.goto(url);

      page.on('dialog', async (dialog) => {
        expect(dialog.type()).toContain('alert');
        expect(dialog.message()).toContain('Error: Invalid file extension: example_jsp');
        await dialog.accept();
      });

      await page.setInputFiles('input[type="file"]', 'assets/files/example_jsp.jsp');
  });

});
