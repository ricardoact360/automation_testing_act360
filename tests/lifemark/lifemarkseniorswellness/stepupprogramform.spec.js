import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://lifemarkseniorswellness.ca/stepupprogramform'

// WIP: Do this.
// NOTE: I should NOT receive an email.
test('Step Up Program Form should submit', async ({ page }) => {
    await page.goto(url);

    const formFields = [
        { label: 'Name', selector: '#edit-name', value: process.env.TESTER_FULLNAME },
        { label: 'Email Address', selector: '#edit-email', value: process.env.TESTER_EMAIL },
        { label: 'Phone number', selector: '#edit-telephone', value: process.env.TESTER_PHONE },
        { label: 'Company', selector: '#edit-subject', value: process.env.TESTER_COMPANY },
        { label: 'Message', selector: '#edit-message', value: process.env.TESTER_MESSAGE }
    ];

    // How can we help you?
    await page.selectOption('#edit-how-can-i-help-you-', 'Medical Assessments');

    // Check empty inputs.
    for (const { _label, selector } of formFields) {
        await page.waitForSelector(selector, { state: 'visible' });
        const locator = page.locator(selector);
        await expect(locator).toBeEmpty({ timeout: 10000 });
    }

    // Fill fields.
    for (const { selector, value } of formFields) {
        await page.fill(selector, value);
    }

    // Verify that the fields have been filled in.
    for (const { selector, value } of formFields) {
        const locator = page.locator(selector);
        await expect(locator).toHaveValue(value, { timeout: 10000 });
    }

    await page.click('#edit-actions-submit');

    // Wait for the page to navigate to the confirmation URL pattern
    await page.waitForURL(/https:\/\/www\.lifemarkhealthgroup\.ca\/form\/contact\/confirmation\?token=.+/);

    // Verify that the page has navigated to the expected URL pattern with a dynamic token
    expect(page.url()).toMatch(/https:\/\/www\.lifemarkhealthgroup\.ca\/form\/contact\/confirmation\?token=.+/);

    await expect(page.locator('text="Thank you for the form submission!"')).toBeVisible();
});


