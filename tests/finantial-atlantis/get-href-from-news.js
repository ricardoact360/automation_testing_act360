import { chromium } from "playwright";
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import dotenv from "dotenv";
dotenv.config();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

(async () => {
  const browser = await chromium.launch({ headless: true });
  const context = await browser.newContext();
  const page = await context.newPage();

  await page.goto('https://atlantisfinancial.ca/in-the-news/');

  const links = await page.evaluate(() => {
    const anchors = Array.from(document.querySelectorAll('a'));
    return anchors
      .filter(a => a.textContent.includes('Read More on MoneySense') || a.textContent.includes('Read More on Financial Post'))
      .map(a => ({
        href: a.href,
        button: a.textContent.includes('Financial Post') ? 'Read More on Financial Post' : 'Read More on MoneySense'
      }));
  });

  const jsonContent = JSON.stringify(links, null, 2);
  const outputPath = path.join(__dirname, 'links.json');
  
  fs.writeFileSync(outputPath, jsonContent);

  console.log(`JSON file has been saved at: ${outputPath}`);

  await browser.close();
})();
