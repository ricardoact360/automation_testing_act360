import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://airdex.actdev.ca/'

test.describe('Airdex Homepage', { tag: '@airdex' }, () => {
  let context;
  let page;

  test.beforeEach(async ({ browser }) => {
    context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
    page = await context.newPage();
    await page.goto(url);
  });

  test.afterEach(async () => { await context.close() });

  test('Header its present', async () => {
    await expect(page.getByRole('heading', { name: 'Expert Manufacturers And' })).toBeVisible();
    await expect(page.getByRole('heading', { name: 'Your Partner For Precision' })).toBeVisible();
    await expect(page.getByRole('button', { name: 'See what Our Clients Say' })).toBeVisible();
    await expect(page.getByRole('button', { name: 'See More Products' })).toBeVisible();
  });

  test('Accordion works', async () => {
    await page.locator('label').filter({ hasText: 'Wholesalers' }).click();
    await expect(page.getByText('For wholesalers who want the')).toBeVisible();
    await page.locator('label').filter({ hasText: 'Commercial HVAC Repair &' }).click();
    await expect(page.getByText('At Airdex, we understand the')).toBeVisible();
    await page.locator('label').filter({ hasText: 'Custom OEM Solutions' }).click();
    await expect(page.getByText('For OEM companies requiring')).toBeVisible();
  });

  /* test('Button to go all up its present', async () => { */
  /*   await expect(page.getByRole('button', { name: '' })).toBeVisible(); */
  /* }); */

  test('Footer has the right data', async () => {
    await expect(page.locator('footer').getByText('Airdex Corporation', { exact: true })).toBeVisible();
    await expect(page.locator('div').filter({ hasText: 'Airdex Corporation 230 Saunders Road Barrie, Ontario L4M 5E2, Canada Phone: (' }).nth(2)).toBeVisible();
    await expect(page.getByText('230 Saunders Road Barrie,')).toBeVisible();
    await expect(page.locator('p').filter({ hasText: 'Phone: (705)' }).getByRole('link')).toBeVisible();
    await expect(page.getByRole('link', { name: '(705) 739.8781' })).toBeVisible();
    await expect(page.getByRole('main').getByRole('link', { name: '(877)' })).toBeVisible();
    await expect(page.getByRole('button', { name: 'About Airdex', exact: true })).toBeVisible();
    await expect(page.getByRole('button', { name: 'Privacy Policy' })).toBeVisible();
    await expect(page.getByRole('button', { name: 'Site Map' })).toBeVisible();
    await expect(page.getByText('Proud subsidiary of')).toBeVisible();
    await expect(page.getByRole('img', { name: 'Nailor Logo' })).toBeVisible();
    await expect(page.getByText('Innovative HVAC products')).toBeVisible();
    await expect(page.getByText('Copyright © 2024 - Airdex')).toBeVisible();
  });

  test('Years, million percentaje and employees information are correct', async () => {
    await expect(page.getByText('25', { exact: true })).toBeVisible();
    await expect(page.getByText('Years', { exact: true })).toBeVisible();
    await expect(page.getByText('Design, Engineering &')).toBeVisible();
    await expect(page.getByText('2.5')).toBeVisible();
    await expect(page.getByText('Million +')).toBeVisible();
    await expect(page.getByText('Fans, Blowers & OEM Solutions')).toBeVisible();
    await expect(page.getByText('<')).toBeVisible();
    await expect(page.getByText('Percent')).toBeVisible();
    await expect(page.getByText('Real World Manufacturer')).toBeVisible();
    await expect(page.getByText('50+')).toBeVisible();
    await expect(page.getByText('Dedicated Airdex Employees To')).toBeVisible();
  });

});

