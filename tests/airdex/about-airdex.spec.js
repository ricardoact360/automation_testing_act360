import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://airdex.actdev.ca/about-airdex/'

test.describe('About Airdex', { tag: '@airdex' }, () => {
  let context;
  let page;

  test.beforeEach(async ({ browser }) => {
    context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
    page = await context.newPage();
    await page.goto(url);
  });

  test.afterEach(async () => { await context.close() });

  test('Header Our History has de right content', async () => {
    await expect(page.getByRole('heading', { name: 'Our Story' })).toBeVisible();
    await expect(page.getByText('25', { exact: true })).toBeVisible();
    await expect(page.getByText('Years', { exact: true })).toBeVisible();
    await expect(page.getByText('2.5', { exact: true })).toBeVisible();
    await expect(page.getByText('Million+')).toBeVisible();
    await expect(page.getByText('<1%', { exact: true })).toBeVisible();
    await expect(page.getByText('50')).toBeVisible();
    await expect(page.locator('#block_acf-22')).toContainText('Design, Engineering & Manufacturing Experience');
    await expect(page.locator('#block_acf-22')).toContainText('Fans, Blowers & OEM Solutions Shipped In North America');
    await expect(page.locator('#block_acf-22')).toContainText('Real World Manufacturer Failure Rate');
    await expect(page.locator('#block_acf-22')).toContainText('Dedicated Airdex Employees To Serve You');
    await expect(page.getByRole('heading', { name: 'Decades of Excellence in HVAC' })).toBeVisible();
    await expect(page.getByText('Airdex’s journey in the HVAC')).toBeVisible();
    await expect(page.getByRole('heading', { name: 'Commitment to Excellence in' })).toBeVisible();
    await expect(page.getByText('At Airdex, our brand promise')).toBeVisible();
  });

  test('Card Section has the right content', async () => {
    await expect(page.getByRole('heading', { name: 'Commitment to Excellence in' })).toBeVisible();
    await expect(page.getByText('At Airdex, our brand promise')).toBeVisible();
    await expect(page.getByText('01', { exact: true })).toBeVisible();
    await expect(page.getByRole('heading', { name: 'Collaboration' })).toBeVisible();
    await expect(page.getByText('At the heart of Airdex is a')).toBeVisible();
    await expect(page.getByText('02', { exact: true })).toBeVisible();
    await expect(page.getByRole('heading', { name: 'Unwavering Excellence' })).toBeVisible();
    await expect(page.getByText('Our commitment to excellence')).toBeVisible();
    await expect(page.getByText('03')).toBeVisible();
    await expect(page.getByRole('heading', { name: 'Customer First' })).toBeVisible();
    await expect(page.getByText('Our clients are the focal')).toBeVisible();
    await expect(page.getByText('04')).toBeVisible();
    await expect(page.getByRole('heading', { name: 'Reliability' })).toBeVisible();
    await expect(page.getByText('Airdex is built on a')).toBeVisible();
    await expect(page.getByRole('article')).toContainText('Need A Quote? We’re Here To Help.');
    await expect(page.getByRole('article')).toContainText('Request A Quote');
  });

});
