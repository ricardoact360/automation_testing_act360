import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://airdex.actdev.ca/about-airdex/contact-us/'

test('Contact Us form should work', async ({ browser }) => {
    const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
    const page = await context.newPage();

    await page.goto(url);

    const formFields = [
        { label: 'First Name', selector: '#input_1_3', value: process.env.TESTER_NAME },
        { label: 'Email', selector: '#input_1_4', value: process.env.TESTER_EMAIL },
        { label: 'Phone number', selector: '#input_1_5', value: process.env.TESTER_PHONE },
        { label: 'Message', selector: '#input_1_6', value: process.env.TESTER_MESSAGE }
    ];

    // Check empty inputs.
    for (const { _label, selector } of formFields) {
        await page.waitForSelector(selector, { state: 'visible' });
        const locator = page.locator(selector);
        await expect(locator).toBeEmpty({ timeout: 10000 });
    }

    // Fill fields.
    for (const { selector, value } of formFields) {
        await page.fill(selector, value);
    }

    // Verify that the fields have been filled in.
    for (const { selector, value } of formFields) {
        const locator = page.locator(selector);
        await expect(locator).toHaveValue(value, { timeout: 10000 });
    }

    await page.click('#gform_submit_button_1');

    await expect(page.locator('#gform_confirmation_message_1')).toContainText('Thanks for contacting us! We will get in touch with you shortly.');
});

