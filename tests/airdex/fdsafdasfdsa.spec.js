import { test, expect } from "@playwright/test";

test('This test will always fail', async ({ page }) => {
  await page.goto('https://www.google.com/');
  await expect(page.locator('h1')).toHaveText('Fail');
});
