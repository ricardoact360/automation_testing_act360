import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://airdex.actdev.ca/'

test.use({
  viewport: { height: 800, width: 360 }
});

test.describe('Airdex Navbar', { tag: '@airdex' }, () => {
  let context;
  let page;

  test.beforeEach(async ({ browser }) => {
    context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
    page = await context.newPage();
    await page.goto(url);
  });

  test.afterEach(async () => { await context.close() });

  test('Navbar should work and have the right elements', async () => {
    await expect(page.getByLabel('Toggle Menu')).toBeVisible();
    await page.getByLabel('Toggle Menu').click();
    await expect(page.getByRole('link', { name: 'Products', exact: true })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Wholesale', exact: true })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Commercial', exact: true })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Custom OEM', exact: true })).toBeVisible();
    await expect(page.getByRole('link', { name: 'News' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'FAQs' })).toBeVisible();
    await expect(page.locator('#mega-menu-item-484').getByRole('link', { name: '(877)' })).toBeVisible();
    await page.getByRole('link', { name: 'About Airdex ' }).click();
    await expect(page.getByRole('link', { name: 'Contact Us' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Prototyping' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Manufacturing' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Testing & Analysis' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Careers', exact: true })).toBeVisible();
  });

});
