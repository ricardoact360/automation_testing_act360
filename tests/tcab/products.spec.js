import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://uat.thecallandbeyond.com/'

test.describe('TCAB Product', () => {
  test('Should able to create and publish a product', async ({ browser }) => {
    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD}
    });
    const page = await context.newPage();

    await page.goto(url);

    await page.waitForTimeout(5000) // Wait 

    await page.click('button[data-tooltip-content="Sign In"]');
    await page.locator('#email').fill('Ricardo@act360.ca');
    await page.locator('#password').fill('your_super_strong_password_here');
    await page.getByRole('button', { name: 'Log in' }).click();

    await page.waitForTimeout(5000) // Wait 
   
    await page.locator('button:has(svg)').nth(1).click();

    // Tienes que cliquear en el icono de la tienda
  });
});
