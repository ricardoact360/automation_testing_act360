import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://uat.thecallandbeyond.com/'

test.describe('Upload Wix-CSV', () => {

  test('Should able to upload Wix-CSV with products', async ({ browser }) => {
    // Increase timeout in 2 minutes
    test.setTimeout(60 * 1000 * 3);

    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD }
    });
    const page = await context.newPage();

    await page.goto(url);

    await page.waitForTimeout(5000)

    await page.click('button.absolute.bg-secondary.rounded-full');
    await page.waitForTimeout(1000);
    await page.locator('button[data-tooltip-content="Sign In"]').nth(1).click();
    await page.waitForTimeout(1000);

    await page.type('input#email[type="email"][placeholder="Email"][required]', 'Ricardo@act360.ca');
    await page.fill('#password', 'your_super_strong_password_here');
    await page.getByRole('button', { name: 'Log in' }).click();

    await page.waitForTimeout(10000) // Wait 

    await page.goto(`${url}vendor/products`);

    await page.getByRole('button', { name: 'Import from CSV' }).click();
    await page.getByRole('button', { name: 'Import New' }).click();
    await page.locator('label').filter({ hasText: 'Wix' }).locator('span').first().click();
    await page.locator('input[type="file"]').setInputFiles('assets/files/wix_example_csv.csv');

    await page.getByRole('button', { name: 'Import', exact: true }).click();

    await page.waitForTimeout(90000);

    await page.goto(`${url}vendor/products`);


    await expect(page.getByRole('link', { name: 'CanAm Flag Shelf Display' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Live Edge Serving Tray' })).toBeVisible();
    await expect(page.getByRole('link', { name: 'Deluxe Duty Rack' })).toBeVisible();
  });


  test('Remove Wix-CSV products', async ({ browser }) => {
    // Aumentar el timeout de la prueba a 2 minutos
    test.setTimeout(60 * 1000 * 3);

    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD }
    });
    const page = await context.newPage();

    await page.goto(url);

    await page.waitForTimeout(5000)

    await page.click('button.absolute.bg-secondary.rounded-full');
    await page.waitForTimeout(1000);
    await page.locator('button[data-tooltip-content="Sign In"]').nth(1).click();
    await page.waitForTimeout(1000);

    await page.type('input#email[type="email"][placeholder="Email"][required]', 'Ricardo@act360.ca');
    await page.fill('#password', 'your_super_strong_password_here');
    await page.getByRole('button', { name: 'Log in' }).click();

    await page.waitForTimeout(10000) // Wait 

    await page.goto(`${url}vendor/products`);

    // NOTE: Removing items...
    page.on('dialog', async dialog => {
      expect(dialog.type()).toContain('confirm'); 
      expect(dialog.message()).toContain('Are you sure?');
      await dialog.accept();
    });

    try {
      await page.getByRole('row', { name: 'CanAm Flag Shelf Display CA$' }).locator('svg').nth(1).click();
      await page.waitForSelector('button:has-text("Archive")', { state: 'visible', timeout: 10000 });
      await page.click('button:has-text("Archive")');

      await page.getByRole('row', { name: 'Live Edge Serving Tray CA$75.' }).locator('svg').nth(1).click();
      await page.waitForSelector('button:has-text("Archive")', { state: 'visible', timeout: 10000 });
      await page.click('button:has-text("Archive")');

      await page.getByRole('row', { name: 'Deluxe Duty Rack CA$350.00 0' }).locator('svg').nth(1).click();
      await page.waitForSelector('button:has-text("Archive")', { state: 'visible', timeout: 10000 });
      await page.click('button:has-text("Archive")');
    } catch (error) {
      console.error('Error during operation:', error);
      throw error;
    }

  });
});
