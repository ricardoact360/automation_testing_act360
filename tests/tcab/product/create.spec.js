import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://uat.thecallandbeyond.com/'

function generateRandomNumber() {
  const min = 10000000000;
  const max = 12012000000;
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

test.describe('TCAB Product', () => {

  test('Should able to create and publish a product', async ({ browser }) => {
    // Increase timeout in 2 minutes
    test.setTimeout(60 * 1000 * 2);

    const randomNumber = generateRandomNumber()
    const productName = `Example Product ${randomNumber}`

    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD }
    });
    const page = await context.newPage();

    try {
      await page.goto(url);

      await page.waitForTimeout(5000)

      await page.click('button.absolute.bg-secondary.rounded-full');
      await page.waitForTimeout(1000);
      await page.locator('button[data-tooltip-content="Sign In"]').nth(1).click();
      await page.waitForTimeout(1000);

      await page.type('input#email[type="email"][placeholder="Email"][required]', 'Ricardo@act360.ca');
      await page.fill('#password', 'your_super_strong_password_here');
      await page.getByRole('button', { name: 'Log in' }).click();
      console.log('Login completed!');

      await page.waitForTimeout(10000) // Wait 

      await page.goto(`${url}vendor/products`);
      console.log('Navigation to vendor/products completed');

      await page.click('a:has-text("Add")');

      await page.fill('textarea[required]', 'Product test must be deleted');

      await page.getByRole('button', { name: 'Select a category' }).click();
      await page.getByRole('button', { name: 'Coffee' }).click();

      console.log('------------------')
      console.log('Take API response')
      console.log('------------------')

      // Interceptar la respuesta del GET que sigue al POST
      const [response] = await Promise.all([
        page.waitForResponse(response => {
          console.log(`Response received: ${response.url()} - Method: ${response.request().method()} - Status: ${response.status()}`);
          return response.url().includes('/vendor/products') && 
                 response.request().method() === 'GET' &&
                 response.status() === 200;
        }),
        page.click('button:has-text("Continue")').then(() => console.log('Button continue clicked'))
      ]);

      console.log('Response successfully intercepted');

      // Obtener el cuerpo de la respuesta
      const responseBody = await response.json();

      // Extraer la URL del objeto de respuesta
      const newUrl = responseBody.url;

      console.log('New URL:', newUrl);

      await page.waitForTimeout(3000) // Wait 

      // Navegar a la nueva URL
      await page.goto(`${url}${newUrl.slice(1)}`);
      console.log('Navigation to new URL completada');

      await page.waitForLoadState('networkidle');

      // Continuar con el resto de las acciones
      await page.fill('textarea.relative.block.h-full.w-full', `${productName}`); // Name
      await page.locator('textarea.relative.block.h-full.w-full').nth(1).fill(process.env.TESTER_MESSAGE); // Description
      await page.fill('input[name="price"]', '150'); // Price
      await page.fill('input[name="stock_quantity"]', '150'); // Quantity
      await page.fill('input[name="weight"]', '5'); // Weight
      await page.locator('fieldset').filter({ hasText: 'Where We\'ll' }).locator('span').first().click();
      await page.locator('label').filter({ hasText: 'Everywhere' }).locator('span').first().click();
      await page.getByRole('button', { name: 'Publish0' }).click();

      await page.waitForTimeout(13000) // Wait 

      await page.goto(`${url}vendor/products`); // Go to products
      await expect(page.locator('tbody')).toContainText(`${productName}`);

    } catch (error) {
      console.error('Error during test:', error);
      throw error;
    }
  });
});
