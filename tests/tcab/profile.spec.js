import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://staging.tcab.app/'

test.describe('TCAB Profile section', () => {
  test('Should able to go to profile and setings when is Login', async ({ browser }) => {
    const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
    const page = await context.newPage();

    await page.goto(url);

    await page.waitForTimeout(5000) // Wait 

    await page.getByRole('button', { name: 'Sign in' }).click();
    await page.locator('#email').fill(process.env.TCAB_USERNAME);
    await page.locator('#password').fill(process.env.TCAB_PASSWORD);
    await page.getByRole('button', { name: 'Log in' }).click();

    await page.waitForTimeout(5000) // Wait 
   
    await page.locator('button:has(svg)').nth(1).click();

    await page.getByText('View your profile').click();

    await expect(page).toHaveURL('https://staging.tcab.app/profile/me');

    await page.getByText('Edit your profile').click();

    await expect(page).toHaveURL('https://staging.tcab.app/user/profile');
  });

  test('Should go to login if try to access to profile when is logout', async ({ browser }) => {
    const context = await browser.newContext({ httpCredentials: { username: '', password: '' } });
    const page = await context.newPage();

    await page.goto('https://staging.tcab.app/profile/me');

    await expect(page).toHaveURL('https://staging.tcab.app/login');

    await page.goto('https://staging.tcab.app/profile/me');

    await expect(page).toHaveURL('https://staging.tcab.app/login');
  });
});
