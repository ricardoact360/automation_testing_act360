import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://uat.thecallandbeyond.com/'

test('Should able to log in', async ({ browser }) => {
  const context = await browser.newContext({ 
    httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD}
  });
  const page = await context.newPage();

  await page.goto(url);

  await page.waitForTimeout(5000)

  await page.click('button.absolute.bg-secondary.rounded-full');
  await page.waitForTimeout(1000);
  await page.locator('button[data-tooltip-content="Sign In"]').nth(1).click();
  await page.waitForTimeout(1000);

  await page.type('input#email[type="email"][placeholder="Email"][required]', process.env.TCAB_PERSONAL_USERNAME);
  await page.fill('#password', process.env.TCAB_PERSONAL_PASSWORD);
  await page.getByRole('button', { name: 'Log in' }).click();
  console.log('Login success');

  await page.click("button[class='w-full py-1.5 flex items-center gap-x-1.5 text-sm rounded-full focus:outline-none focus:site-border dark:focus:border-chrome-800 transition site-link'] svg");
  await expect(page.getByRole('link', { name: 'Edit your profile' })).toBeVisible();
  await expect(page.getByRole('link', { name: 'Purchases' })).toBeVisible();
  await expect(page.getByRole('link', { name: 'Messages' })).toBeVisible();
  await expect(page.getByRole('button', { name: 'Logout' })).toBeVisible();
});

