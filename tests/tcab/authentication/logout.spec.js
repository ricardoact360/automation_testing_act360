import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://uat.thecallandbeyond.com/'

test('Should able to log out', async ({ browser }) => {
  const context = await browser.newContext({ 
    httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD}
  });
  const page = await context.newPage();

  await page.goto(url);

  await page.waitForTimeout(5000)

  await page.click('button.absolute.bg-secondary.rounded-full');
  await page.waitForTimeout(1000);
  await page.locator('button[data-tooltip-content="Sign In"]').nth(1).click();
  await page.waitForTimeout(1000);

  await page.type('input#email[type="email"][placeholder="Email"][required]', process.env.TCAB_PERSONAL_USERNAME);
  await page.fill('#password', process.env.TCAB_PERSONAL_PASSWORD);
  await page.getByRole('button', { name: 'Log in' }).click();
  console.log('Login success');

  await page.waitForTimeout(10000) // Wait 

  await page.getByRole('button', { name: 'Richard' }).click();
  await page.getByRole('button', { name: 'Logout' }).click();
  await expect(page.getByRole('heading', { name: 'Log into Your Account' })).toBeVisible();

});
