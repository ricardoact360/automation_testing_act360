import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://uat.thecallandbeyond.com/'
const emailUrl = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${process.env.MAILNATOR_ID}`

function generateRandomNumber() {
  const min = 10000000000;
  const max = 12012000000;
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

test.describe('Create an User and receive email', () => {

  test('Should able to create an User & receive verification email', async ({ browser }) => {

    const randomNumber = generateRandomNumber()
    const actualEmail = `${randomNumber}@mailinator.com`

    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD}
    });
    const page = await context.newPage();

    await page.goto(url);

    await page.waitForTimeout(10000)

    await page.click('button.absolute.bg-secondary.rounded-full');
    await page.waitForTimeout(1000);
    await page.locator('button[data-tooltip-content="Sign In"]').nth(1).click();
    await page.waitForTimeout(1000);
    await page.click('button.site-link:has-text("Register")');

    console.log('actualEmail: ', actualEmail)

    await page.fill('#name', 'John Doe');
    await page.locator('#email').nth(2).fill(actualEmail);
    await page.waitForTimeout(2000);
    await page.fill('#password', 'your_super_strong_password_here');
    await page.fill('#password_confirmation', process.env.TCAB_PERSONAL_PASSWORD);
    await page.locator('label').filter({ hasText: 'I agree to the Terms of' }).locator('span').first().click();
    await page.waitForTimeout(2000);
    await page.getByRole('button', { name: 'Register' }).click();

    await page.waitForTimeout(10000);

    const actualEmailUrl = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${randomNumber}`
    console.log('actualEmailUrl: ', actualEmailUrl)
    await page.goto(actualEmailUrl);
    await page.waitForTimeout(4000);
    await page.getByRole('cell', { name: 'Verify Email Address' }).first().click();
    await expect(page.locator('#email_pane')).toContainText('thecallandbeyond@gmail.com');
  });

});

