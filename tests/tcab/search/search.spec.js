import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://uat.thecallandbeyond.com/'

test.describe('TCAB Seach funcitonality', () => {

  test('Should able to search and find a product', async ({ browser }) => {
    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD }
    });
    const page = await context.newPage();

    await page.goto(url);

    // Removing banner
    await page.click('button.absolute.bg-secondary.rounded-full');
    await page.waitForTimeout(1000);

    await page.getByPlaceholder('SEARCH').click();
    await page.getByPlaceholder('SEARCH').fill('guitar');
    await page.getByPlaceholder('SEARCH').press('Enter');
    await expect(page.getByRole('link', { name: 'Fender acoustic guitar', exact: true })).toBeVisible();
  });

  test.skip('Should able to search with filters', async ({ browser }) => {
    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD }
    });
    const page = await context.newPage();

    await page.goto(url);

    // Removing banner
    await page.click('button.absolute.bg-secondary.rounded-full');
    await page.waitForTimeout(1000);

    await page.getByPlaceholder('SEARCH').click();
    await page.getByPlaceholder('SEARCH').fill('guitar');
    await page.getByPlaceholder('SEARCH').press('Enter');
    await expect(page.getByRole('link', { name: 'Fender acoustic guitar', exact: true })).toBeVisible();
    
    // TODO: Create multiple products in a isolated account.
    // TODO: Create this test.
  });

});
