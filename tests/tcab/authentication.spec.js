import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://uat.thecallandbeyond.com/'

test.describe('TCAB Authentication', () => {

  test.skip('Should login and go to Profile', async ({ browser }) => {
    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD }
    });
    const page = await context.newPage();

    await page.goto(url);

    await page.waitForTimeout(5000)

    await page.click('button.absolute.bg-secondary.rounded-full');
    await page.waitForTimeout(1000);
    await page.click('button[data-tooltip-content="Sign In"]');
    await page.waitForTimeout(1000);

    await page.type('input#email[type="email"][placeholder="Email"][required]', 'Ricardo@act360.ca');
    await page.fill('#password', 'your_super_strong_password_here');
    await page.getByRole('button', { name: 'Log in' }).click();

    await page.waitForTimeout(10000) // Wait 

    await page.goto(`${url}profile/me`);

    await expect(page.locator('text="Edit your profile"')).toBeVisible();
  });
  

  test('Should be able to login and logout', async ({ browser }) => {
    const context = await browser.newContext({ 
      httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD }
    });
    const page = await context.newPage();

    await page.goto(url);

    await page.waitForTimeout(5000)

    await page.click('button.absolute.bg-secondary.rounded-full');
    await page.waitForTimeout(1000);
    await page.click('button[data-tooltip-content="Sign In"]');
    await page.waitForTimeout(1000);

    await page.type('input#email[type="email"][placeholder="Email"][required]', 'Ricardo@act360.ca');
    await page.fill('#password', 'your_super_strong_password_here');
    await page.getByRole('button', { name: 'Log in' }).click();

    await page.waitForTimeout(10000) // Wait 

    await page.click('button.w-full.py-1\\.5.flex.items-center.gap-x-1\\.5.text-sm.rounded-full');

    await page.getByText('Logout').click();


    await page.waitForTimeout(5000) // Wait 
    await page.waitForSelector('button[data-tooltip-content="Sign In"]', { state: 'visible' });

  });
});
