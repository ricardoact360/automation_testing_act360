import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const url = 'https://thecallandbeyond.com/'

test('rrrrrrrrrrrrrrrrrrrrrrrrr', async ({ browser }) => {
  const context = await browser.newContext({ 
    httpCredentials: { username: process.env.TCAB_USERNAME, password: process.env.TCAB_PASSWORD}
  });
  const page = await context.newPage();

  await page.goto(url);

  await page.waitForTimeout(5000)

  await page.click('button.absolute.bg-secondary.rounded-full');
  await page.waitForTimeout(1000);
  await page.locator('button[data-tooltip-content="Sign In"]').nth(1).click();
  await page.waitForTimeout(3000);

  await page.type('input#email[type="email"][placeholder="Email"][required]', process.env.TCAB_MASTER_USERNAME);
  await page.fill('#password', process.env.TCAB_MASTER_PASSWORD);
  await page.getByRole('button', { name: 'Log in' }).click();
  console.log('Login success');

  await page.waitForTimeout(5000);

  await page.goto(`https://thecallandbeyond.com/admin/marketing/sales/7`);

  await page.waitForTimeout(8000);

  const nameLinks = await page.locator('tbody td.name-cell a').all();
  
  for (const link of nameLinks) {
    const href = await link.getAttribute('href');
    console.log(href)
    if (href) {
      const newPage = await context.newPage();
      await newPage.goto(`${url}${href}`);
      await page.waitForTimeout(1000);
    }
  } 
});


/*

  // NOTE: For codegen

(async () => {
  const url = 'https://thecallandbeyond.com'
  const browser = await chromium.launch({ headless: false });
  const context = await browser.newContext();
  const page = await context.newPage();
  await page.goto(url);

  await page.waitForTimeout(5000)

  await page.click('button.absolute.bg-secondary.rounded-full');
  await page.waitForTimeout(1000);
  await page.locator('button[data-tooltip-content="Sign In"]').nth(1).click();
  await page.waitForTimeout(3000);

  await page.type('input#email[type="email"][placeholder="Email"][required]', process.env.TCAB_MASTER_USERNAME);
  await page.fill('#password', process.env.TCAB_MASTER_PASSWORD);
  await page.getByRole('button', { name: 'Log in' }).click();
  console.log('Login success');

  await page.waitForTimeout(5000);

  await page.goto(`https://thecallandbeyond.com/admin/marketing/sales/7`);

  await page.waitForTimeout(8000);

  const nameLinks = await page.locator('tbody td.name-cell a').all();
  
  for (const link of nameLinks) {
    const href = await link.getAttribute('href');
    console.log(href)
    if (href) {
      const newPage = await context.newPage();
      await newPage.goto(`${url}${href}`);
      await page.waitForTimeout(1000);
    }
  } 
})();

*/
