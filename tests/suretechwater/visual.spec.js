import { test, expect } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

test.describe('Suretechwater', { tag: '@suretechwater' }, () => {

  test('Suretechwater homepage should exist', async ({ page }) => {
      await page.goto( 'https://suretechwater.ca/');

      await expect(page.getByText('Home')).toBeVisible();
      await expect(page.getByRole('link', { name: 'Services' })).toBeVisible();
      await expect(page.getByRole('link', { name: 'Water Filter, Systems & Parts' })).toBeVisible();
      await expect(page.getByText('WHAT WE DO')).toBeVisible();
      await expect(page.getByRole('link', { name: 'Installation & Repair of' })).toBeVisible();
      await expect(page.getByRole('link', { name: 'Shipping' })).toBeVisible();
      await expect(page.getByRole('heading', { name: 'OUR CLIENTS' })).toBeVisible();
  });

  test('Suretechwater Services should exist', async ({ page }) => {
      await page.goto( 'https://suretechwater.ca/services');
      await expect(page.getByRole('link', { name: 'Installation & Repair of' })).toBeVisible();
      await expect(page.getByRole('link', { name: 'Prescheduled Filter Service' })).toBeVisible();
      await expect(page.getByRole('heading', { name: 'Sales' }).getByRole('link')).toBeVisible();
  });

  test('Suretechwater About Us should exist', async ({ page }) => {
      await page.goto( 'https://suretechwater.ca/about-us');

      await expect(page.getByRole('heading', { name: 'Who is Suretech?' })).toBeVisible();
      await expect(page.getByText('Definition of persistence')).toBeVisible();
      await expect(page.getByText('1: firm or obstinate')).toBeVisible();
      await expect(page.getByText('2: the continued or prolonged')).toBeVisible();

      await expect(page.getByText('“Nothing in the world can')).toBeVisible();
      await expect(page.getByText('Talent will not; nothing is')).toBeVisible();
      await expect(page.getByText('men with talent. Genius will')).toBeVisible();
      await expect(page.getByText('a proverb. Education will not')).toBeVisible();
  });
});

