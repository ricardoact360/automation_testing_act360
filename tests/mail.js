import nodemailer from "nodemailer";
// NOTE: api_key_brevo 
// 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
import brevo from "@getbrevo/brevo";
import fs from "fs";

const apiInstance = new brevo.TransactionalEmailsApi();

apiInstance.setApiKey(
  brevo.TransactionalEmailsApiApiKeys.apiKey,
  "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"
);

async function main() {
  try {
    const sendSmtpEmail = new brevo.SendSmtpEmail();

    sendSmtpEmail.sender = { name: "Ricardo Aguilar", email: "Ricardo@act360.ca" };
    sendSmtpEmail.to = [
      { email: "Ricardoaguilar96@yahoo.com", name: "Ricardo Aguilar" },
      { email: "adam@act360.com", name: "Adam Bowles" },
      { email: "billy@act360.com", name: "Billy Pollock" },
    ];

    sendSmtpEmail.subject = "Hello, world! from Brevo and Node.js (Testing)";
    sendSmtpEmail.htmlContent = 'This is a test, with a file attached'

    const fileCSV = fs.readFileSync('./files/cutomers_list.csv');
    const csvBase64 = fileCSV.toString('base64');

    sendSmtpEmail.attachment = [{
      "content": csvBase64,
      "name": "customer_list.csv",
      "type": "text/csv"  
    }]

    const result = await apiInstance.sendTransacEmail(sendSmtpEmail);

    console.log('Email was send!');
    /* console.log(result); */
  } catch (error) {
    console.error("There is an error!")
    console.error(error);
  }
}

main();
