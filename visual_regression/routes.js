// const prodPage = "https://stoneplace.com";
// const devPage = "https://master.stoneplace.actdev.ca";
export const routeStonePlace = [
    { fileName: 'home', routeProd: '/' },
    { fileName: 'contact', routeProd: '/contact/' },
    { fileName: 'our-company', routeProd: '/our-company/' },
    { fileName: 'our-suppliers', routeProd: '/our-suppliers/' },
    { fileName: 'recommended-contractors', routeProd: '/recommended-contractors/' },
    { fileName: 'delivery-services', routeProd: '/delivery-services/' },
    { fileName: 'calculator', routeProd: '/calculator/' },
];

// https://www.lifemarkworkhealth.ca
// https://upcore.branchmanager.lifemarkworkhealth.ca
export const routeLifemarkworkhealth = [
    { fileName: 'home', routeProd: '/' },
    { fileName: 'occupational-health', routeProd: '/occupational-health' },
    { fileName: 'workplace-wellness', routeProd: '/workplace-wellness' },
    { fileName: 'disability-management', routeProd: '/disability-management' },
    { fileName: 'independent-medical-examinations', routeProd: '/independent-medical-examinations' },
    { fileName: 'our-locations', routeProd: '/our-locations' },
    { fileName: 'about-us', routeProd: '/about-us' },
    { fileName: 'careers', routeProd: '/careers' },
    { fileName: 'contact-us', routeProd: '/contact-us' },
    { fileName: 'search', routeProd: '/search?criteria=Toronto&proximity-lat=43.653226&proximity-lng=-79.3831843&proximity=30'},
];

// https://www.lifemarksantetravail.ca
// https://upcorefr.branchmanager.lifemarkworkhealth.ca
export const routeLifemarkworkhealthFr = [
    { fileName: 'home', routeProd: '/' },
    { fileName: 'sante-au-travail', routeProd: '/sante-au-travail' },
    { fileName: 'sante-et-mieux-etre-en-milieu-de-travail', routeProd: '/sante-et-mieux-etre-en-milieu-de-travail' },
    { fileName: 'gestion-de-linvalidite', routeProd: '/gestion-de-linvalidite' },
    { fileName: 'examens-medicaux-independants', routeProd: '/examens-medicaux-independants' },
    { fileName: 'nos-emplacements', routeProd: '/nos-emplacements' },
    { fileName: 'propos-de-nous', routeProd: '/propos-de-nous' },
    { fileName: 'carrieres', routeProd: '/carrieres' },
    { fileName: 'nous-contacter', routeProd: '/nous-contacter' },
    { fileName: 'search', routeProd: '/search?criteria=Toronto&proximity-lat=43.653226&proximity-lng=-79.3831843&proximity=30' },
];

// https://lifemarkhealthgroup.ca
// https://www.upcore.branchmanager.lifemarkhealthgroup.ca
export const routeLifemarkhealthgroup = [
    { fileName: 'home', routeProd: '/' },
    { fileName: 'step-by-step-guidance', routeProd: '/join-lifemark/step-by-step-guidance' },
    { fileName: 'join-lifemark', routeProd: '/join-lifemark' },
    { fileName: 'testimonials', routeProd: '/join-lifemark/testimonials' },
    { fileName: 'services', routeProd: '/services' },
    { fileName: 'careers', routeProd: '/careers' },
    { fileName: 'contact-us', routeProd: '/contact-us' },
    { fileName: 'testimonials', routeProd: '/acquisition-opportunities/testimonials' },
    { fileName: 'step-by-step-guidance', routeProd: '/acquisition-opportunities/step-by-step-guidance' },
    { fileName: 'services-workplace-health-and-wellness', routeProd: '/services/workplace-health-and-wellness' },
];

// https://lifeisnow.ca
// https://uat.lifeisnow.ca
export const routeLifeisnow = [
    { fileName: 'home', routeProd: '/' },
    { fileName: 'first-five-steps-free', routeProd: '/courses/first-five-steps-free/' },
    { fileName: 'step-1-gain-knowledge', routeProd: '/sections/step-1-gain-knowledge/' },
    { fileName: 'step-2-breathing', routeProd: '/sections/step-2-breathing/' },

    { fileName: 'pain-care-for-life-6-months', routeProd: '/product/pain-care-for-life-6-months/' },
    { fileName: 'pain-care-for-life', routeProd: '/courses/pain-care-for-life/' },

    { fileName: 'about', routeProd: '/about/' },
    { fileName: 'contact', routeProd: '/contact/' },
    { fileName: 'shop', routeProd: '/shop/' },
];


// https://fitforwork.com
// https://uat.fitforwork.com
export const routeFitforwork = [
    { fileName: 'home', routeProd: '/' },
    { fileName: 'habout-ffwome', routeProd: '/about-ffw' },
    { fileName: 'about-ffw-addiliates', routeProd: '/about-ffw/affiliates' },
    { fileName: 'services', routeProd: '/services' },
    { fileName: 'treatment-rehabilitation-services', routeProd: '/services/treatment-rehabilitation-services' },
    { fileName: 'contact-contact-us', routeProd: '/contact/contact-us' },
    { fileName: 'customer-login', routeProd: '/customer-login' },
];

// https://www.metricsvocational.ca
// https://www.uat.metricsvocational.ca
export const routeMetricsVocational = [
    { fileName: 'service-request', routeProd: '/service-request/' },

];

// https://www.sportmedspecialistsbrampton.com
export const routeSportmedspecialistsbrampton = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'services-physiotherapy', routeProd: '/services/physiotherapy' },
    { fileName: 'services-massage-therapy', routeProd: '/services/massage-therapy' },
    { fileName: 'services-acupuncture', routeProd: '/services/acupuncture' },
    { fileName: 'services-custom-orthotics', routeProd: '/services/custom-orthotics' },
    { fileName: 'services-sports-physicians', routeProd: '/services/sports-physicians' },
    { fileName: 'services-orthopaedic-surgeons', routeProd: '/services/orthopaedic-surgeons' },
    { fileName: 'services-motor-vehicle-accident-program', routeProd: '/services/motor-vehicle-accident-program' },
    { fileName: 'services-orthopaedic-bracing', routeProd: '/services/orthopaedic-bracing' },
    { fileName: 'about-sms', routeProd: '/about-sms' },
    { fileName: 'our-team', routeProd: '/our-team' },
    { fileName: 'our-team-physicians', routeProd: '/our-team/physicians' },
    { fileName: 'our-team-orthopaedic-surgeons', routeProd: '/our-team/orthopaedic-surgeons' },
    { fileName: 'our-team-physiotherapists', routeProd: '/our-team/physiotherapists' },
    { fileName: 'our-team-massage-therapists', routeProd: '/our-team/massage-therapists' },
    { fileName: 'our-team/pedorthists', routeProd: '/our-team/pedorthists' },
    { fileName: 'facility', routeProd: '/facility' },
    { fileName: 'careers', routeProd: '/careers' },
    { fileName: 'links', routeProd: '/links' },
    { fileName: 'contact-us', routeProd: '/contact-us' },
    { fileName: 'booking-appointments', routeProd: '/booking-appointments' },
];

// https://moldparts.com
// https://actmaster.moldparts.actdev.ca
export const routeMoldparts = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'hot-runner-spare-parts', routeProd: '/hot-runner-spare-parts' },
    { fileName: 'reverse-engineering', routeProd: '/reverse-engineering' },
    { fileName: 'design-custom-parts', routeProd: '/design-custom-parts' },
    { fileName: 'about-us', routeProd: '/about-us' },
    { fileName: 'contact-us', routeProd: '/contact-us' },
];

// https://www.avfi.com
// https://www.uat.avfi.com
export const routeAvfi = [
    { fileName: 'avfi-finishes', routeProd: '/avfi-finishes' },
];


// https://www.pthealth.ca
// https://master.dev.pthealth.ca
// NOTE: This page has issue with this Visual Regression Tool, please use: https://diffy.website/
export const routePtHealth = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'services', routeProd: '/services' },
    { fileName: 'services-acupuncture-treatment', routeProd: '/services/acupuncture-treatment/' },
    { fileName: 'clinic-page-virtual-care', routeProd: '/clinic-page/virtual-care/' },
    /* { fileName: 'pchealth', routeProd: '/pchealth' }, */
    { fileName: 'about-us', routeProd: '/about-us/' },
    { fileName: 'about-us-what-to-expect/', routeProd: '/about-us/what-to-expect/' },
    { fileName: 'blog', routeProd: '/blog' },
    { fileName: 'physicians', routeProd: '/physicians/' },
    { fileName: 'ppn-employers', routeProd: '/ppn/employers/' },
    { fileName: 'careers', routeProd: '/careers' },
    { fileName: 'student-programs-physiotherapy-bursary-program', routeProd: '/student-programs/physiotherapy-bursary-program/' },
    { fileName: 'careers-volunteer', routeProd: '/careers/volunteer/' },
    { fileName: 'conditions', routeProd: '/conditions/' },
    { fileName: 'search-ontario', routeProd: '/?s=Ontario' },
    /* { fileName: 'find-a-clinic/', routeProd: '/find-a-clinic/' }, */
];

// https://physiotherapieuniverselle.com
// https://uat.physiotherapieuniverselle.com
export const routePhysiotherapieuniverselle = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'cliniques', routeProd: '/cliniques' },
    { fileName: 'services', routeProd: '/services' },
    { fileName: 'services-physiotherapie', routeProd: '/services/physiotherapie' },
    { fileName: 'services-ergotherapie', routeProd: '/services/ergotherapie' },
    { fileName: 'services-acupuncture', routeProd: '/services/acupuncture' },
    { fileName: 'services-servicecat-employeurs', routeProd: '/services?servicecat=employeurs' },
    { fileName: 'services-servicecat-particuliers', routeProd: '/services?servicecat=particuliers' },
    { fileName: 'services-servicecat-assureursmedecins', routeProd: '/services?servicecat=assureursmedecins' },
    { fileName: 'telereadaptation', routeProd: '/telereadaptation' },
    { fileName: 'blogue', routeProd: '/blogue' },
    { fileName: 'covid-19', routeProd: '/covid-19' },
    { fileName: 'carriere', routeProd: '/carriere' },
    { fileName: 'a-propos', routeProd: '/a-propos' },
    { fileName: 'contact', routeProd: '/contact' },
    { fileName: 'search-physi', routeProd: '/?s=Physi' },
    { fileName: 'en-clinics-aylmer-our-team', routeProd: '/en/clinics/aylmer/our-team' },
    { fileName: 'contact-form', routeProd: '/contact' },
];

// https://www.lifemark.ca
// https://uat.lifemark.ca
// https://www.upcore.branchmanager.lifemark.ca
// NOTE: Be aware!" NO top space over "Search for services, condi..."
// TRY IN DIFERENTS DEVICES.
export const routeLifemark = [
    { fileName: 'about', routeProd: '/about-us' },
];

// https://vp-group.ca
// https://www.upcore.branchmanager.vp-group.ca
// https://uat.vp-group.ca
export const routeVpGroup = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'about_us', routeProd: '/about-us' },
    { fileName: 'services', routeProd: '/services' },
    { fileName: 'services-life-and-health-insurers', routeProd: '/services/life-and-health-insurers' },
    { fileName: 'services-property-and-casualty-insurers', routeProd: '/services/property-and-casualty-insurers' },
    { fileName: 'services-legal-community', routeProd: '/services/legal-community' },
    { fileName: 'services-employers-and-government', routeProd: '/services/employers-and-government' },
    { fileName: 'expertise', routeProd: '/expertise' },
    { fileName: 'events-and-resources', routeProd: '/events-and-resources' },
    // { fileName: 'locations', routeProd: 'locations' }, // FIXME: Locations doesn't load well
    { fileName: 'careers', routeProd: '/careers' },
    { fileName: 'careers-employees', routeProd: '/careers/employees' },
    { fileName: 'careers-medical-experts-consultants', routeProd: '/careers/medical-experts-consultants' },
    { fileName: 'contact-us', routeProd: '/contact-us' },
    { fileName: 'contact-us-referral-form', routeProd: '/contact-us/referral-form' },
    { fileName: 'contact-us-secure-document-upload', routeProd: '/contact-us/secure-document-upload' },
    { fileName: 'contact-us-sign-up-for-assessor-availability-circulars', routeProd: '/contact-us/sign-up-for-assessor-availability-circulars' },
];

// https://lifemarkseniorswellness.ca
// https://www.upcore.branchmanager.lifemarkseniorswellness.ca
// https://uat.lifemarkseniorswellness.ca
export const routeLifemarkseniorswellnessEn = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'enhanced-care', routeProd: '/enhanced-care' },
    { fileName: 'freeexerciseclasses', routeProd: '/freeexerciseclasses' },
    { fileName: 'stepupprogram', routeProd: '/stepupprogram' },
    { fileName: 'stepupprogramform', routeProd: '/stepupprogramform' },
];

// https://lifemarkmieuxetredespersonnesainees.ca
// https://www.upcore.branchmanager.lifemarkseniorswellness.ca/fr 
export const routeLifemarkseniorswellnessFr = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'offre-de-soins-superieure', routeProd: '/offre-de-soins-superieure' },
    { fileName: 'coursdexercicesgratuits', routeProd: '/coursdexercicesgratuits' },
    { fileName: 'stepupprogram', routeProd: '/stepupprogram' },
    { fileName: 'stepupprogramform', routeProd: '/stepupprogramform' },
];

// https://www.nurseshealth.ca
// http://www.upcore.branchmanager.nurseshealth.ca
export const routeNurseshealthEn = [
    { fileName: 'about-us', routeProd: '/en/about-us' },
    { fileName: 'en-eligibility-referral', routeProd: '/en/eligibility-referral' },
    { fileName: 'en-how-we-help', routeProd: '/en/how-we-help' },
    { fileName: 'en-resources', routeProd: '/en/resources' },
    { fileName: 'en-faq', routeProd: '/en/faq' },
    { fileName: 'en-contact-us', routeProd: '/en/contact-us' },
    { fileName: 'en-document-upload', routeProd: '/document-upload' },
];


// https://car-rehab.com
// https://uat.car-rehab.com
export const routeCarRehab = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'services', routeProd: '/services' },
    { fileName: 'pediatric-services', routeProd: '/pediatric-services' },
    { fileName: 'yourhealth', routeProd: '/yourhealth' },
    { fileName: 'about-us', routeProd: '/about-us' },
    { fileName: 'patient-bill-of-rights', routeProd: '/patient-bill-of-rights' },
    { fileName: 'privacy-policy-2', routeProd: '/privacy-policy-2' },
    { fileName: 'client-experience', routeProd: '/client-experience' },
    { fileName: 'contact', routeProd: '/contact' },
    { fileName: 'careers', routeProd: '/careers' },
    { fileName: 'careers-current-opportunities', routeProd: '/careers/current-opportunities' },
    { fileName: 'search-ontario', routeProd: '/?s=ontario' },
];

// https://karprehab.com
// https://uat.karprehab.com
export const routeKarprehab = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'vancouver-kinesiology', routeProd: '/vancouver-kinesiology/' },
    { fileName: 'occupational-therapy', routeProd: '/occupational-therapy/' },
    { fileName: 'vancouver-physiotherapy', routeProd: '/vancouver-physiotherapy/' },
    { fileName: 'category-worksite-evaluations', routeProd: '/category/worksite-evaluations' },
    { fileName: 'occupational-therapy-pediatric-occupational-therapy-services', routeProd: '/occupational-therapy/pediatric-occupational-therapy-services/' },
    { fileName: 'languages', routeProd: '/languages' },
    { fileName: 'vancouver-rehabilitation-locations', routeProd: '/vancouver-rehabilitation-locations' },
    { fileName: 'referrals', routeProd: '/referrals' },
    { fileName: 'careers', routeProd: '/careers' },
    { fileName: 'resources', routeProd: '/resources' },
    { fileName: 'vancouver-rehabilitation-contact-information', routeProd: '/vancouver-rehabilitation-contact-information/' },
    { fileName: 'virtual-care', routeProd: '/virtual-care' },
];

// https://www.karpfitness.com
// https://www.uat.karpfitness.com
export const routeFitness = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'services', routeProd: '/services/' },
    { fileName: 'personal-training', routeProd: '/personal-training/' },
    { fileName: 'high-performance-athlete-training', routeProd: '/high-performance-athlete-training/' },
    { fileName: 'corporate-programs', routeProd: '/corporate-programs/' },
    { fileName: 'vancouver-personal-trainer-exercise-guide', routeProd: '/category/vancouver-personal-trainer-exercise-guide/' },
    { fileName: 'fitness-tips', routeProd: '/category/fitness-tips/' },
    { fileName: 'healthy-eating-2', routeProd: '/category/healthy-eating-2/' },
    { fileName: 'video', routeProd: '/category/video/' },
    { fileName: 'vancouver-personal-training-locations', routeProd: '/vancouver-personal-training-locations/' },
    { fileName: 'vancouver-personal-training-contact-information', routeProd: '/vancouver-personal-training-contact-information/' },
    { fileName: 'search-fitness', routeProd: '/?s=fitness' },
];

// https://prophysiotherapy.com
// https://uat.prophysiotherapy.com
export const routeProphysiotherapyEn = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'about', routeProd: '/about.html' },
    { fileName: 'about_team', routeProd: '/about_team.html#1' },
    { fileName: 'preferred_provider', routeProd: '/preferred_provider.html' },
    { fileName: 'about_involvement', routeProd: '/about_involvement.html' },
    { fileName: 'careers', routeProd: '/careers.html' },
    { fileName: 'prophysio_academy', routeProd: '/prophysio_academy/index.html' },
    { fileName: 'sportmedicine', routeProd: '/sportmedicine.html' },
    { fileName: 'osmc', routeProd: '/osmc/index.html' },
    { fileName: 'services', routeProd: '/services.html' },
    { fileName: 'locations', routeProd: '/locations.html' },
    { fileName: 'locations-all-locations', routeProd: '/locations.html#alllocations' },
    { fileName: 'locations-map-locations', routeProd: '/locations.html#maplocations' },
    { fileName: 'locations-area', routeProd: '/locations.html#area' },
    { fileName: 'news', routeProd: '/news.html' },
    { fileName: 'testimonials', routeProd: '/testimonials.html' },
];

// https://glebephysio.com
// https://uat.glebephysio.com
export const routeGlebephysio = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'about', routeProd: '/about-us/' },
    { fileName: 'clinic_space', routeProd: '/clinic-space/' },
    { fileName: 'services', routeProd: '/services' },
    { fileName: 'conditions', routeProd: '/conditions' },
    { fileName: 'virtual-care', routeProd: '/virtual-care' },
    { fileName: 'resources/useful-links-info', routeProd: '/resources/useful-links-info/' },
    { fileName: 'main', routeProd: '/contact-us/' },
    { fileName: 'conditions-faq', routeProd: '/conditions/faq/' },
];


// https://www.pickeringphysio.ca
// https://www.staging.pickeringphysio.ca
export const routePickeringphysio = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'physiotherapy', routeProd: '/physiotherapy' },
    { fileName: 'registered-massage-therapy', routeProd: '/registered-massage-therapy' },
    { fileName: 'pain-management', routeProd: '/pain-management/' },
    { fileName: 'custom-orthotics', routeProd: '/custom-orthotics/' },
    { fileName: 'custom-bracing', routeProd: '/custom-bracing' },
    { fileName: 'our-team', routeProd: '/our-team/' },
    { fileName: 'patient-centre', routeProd: '/patient-centre/' },
    { fileName: 'patient-forms', routeProd: '/patient-forms' },
    { fileName: 'direct-billing-insurance-coverage-information', routeProd: '/direct-billing-insurance-coverage-information' },
    { fileName: 'faq', routeProd: '/faq' },
    { fileName: 'contact-us', routeProd: '/contact-us' },
];

// https://otconsulting.ca
// https://uat.otconsulting.ca
export const routeOtconsulting = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'treatment', routeProd: '/treatment/' },
    { fileName: 'treatment_assessment_services', routeProd: '/treatment/assessment-services/' },
    { fileName: 'treatment_services', routeProd: '/treatment/services/' },
    { fileName: 'treatment_our_team_medical_legal_laura_andrews', routeProd: '/treatment/our-team/medical-legal/laura-andrews/' },
    { fileName: 'treatment_our_team_medical_legal_debbie_scott_kerr', routeProd: '/treatment/our-team/medical-legal/debbie-scott-kerr/' },
    { fileName: 'treatment_about', routeProd: '/treatment/about/' },
    { fileName: 'treatment_careers', routeProd: '/treatment/careers/' },
    { fileName: 'treatment_location', routeProd: '/treatment/location/' },
    { fileName: 'treatment_search_Ontario', routeProd: '/treatment/?s=Ontario' },
];

// https://synergyhealthmanagement.com
// https://dev.synergyhealthmanagement.com 
export const routeSynergyhealthmanagement = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'physiotherapy', routeProd: '/service/physiotherapy/' },
    { fileName: 'chiropractor-victoria-bc', routeProd: '/service/chiropractor-victoria-bc/' },
    { fileName: 'massage-therapy', routeProd: '/service/massage-therapy/' },
    { fileName: 'kinesiology', routeProd: '/service/kinesiology/' },
    { fileName: 'about-synergy', routeProd: '/about-synergy' },
    { fileName: 'our-team', routeProd: '/our-team/' },
    { fileName: 'blog', routeProd: '/blog/' },
    { fileName: 'contact', routeProd: '/contact/' },
];

// https://corerehab.ca
// https://uat.corerehab.ca
export const routeCorerehab = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'Forms', routeProd: '/Forms.html' },
    { fileName: 'About', routeProd: '/About.html' },
    { fileName: 'Services', routeProd: '/Services.html' },
    { fileName: 'Resources', routeProd: '/Resources.html' },
    { fileName: 'Contact', routeProd: '/Contact.html' },
];

// https://mytorontophysio.com
// https://uat.mytorontophysio.com
export const routeMytorontophysio = [
    { fileName: 'main', routeProd: '/' },
    { fileName: 'virtual-care-with-my-toronto-physio', routeProd: '/virtual-care-with-my-toronto-physio/' },
    { fileName: 'services', routeProd: '/services/' },
    { fileName: 'acupuncture-therapy-treatment-toronto', routeProd: '/services/acupuncture-therapy-treatment-toronto/' },
    { fileName: 'chiropody-toronto', routeProd: '/services/chiropody-toronto/' },
    { fileName: 'concussion-vestibular-physiotherapy-toronto', routeProd: '/services/concussion-vestibular-physiotherapy-toronto/' },
    { fileName: 'ims-treatment-dry-needling-toronto', routeProd: '/services/ims-treatment-dry-needling-toronto/' },
    { fileName: 'programs', routeProd: '/programs/' },
    { fileName: 'post-covid-19-rehabilitation-and-recovery-program-toronto', routeProd: '/programs/post-covid-19-rehabilitation-and-recovery-program-toronto/' },
    { fileName: 'custom-products', routeProd: '/custom-products/' },
    { fileName: 'custom-bracing', routeProd: '/custom-products/custom-bracing/' },
    { fileName: 'custom-orthotics', routeProd: '/custom-products/custom-orthotics/' },
    { fileName: 'therapy-supplies', routeProd: '/custom-products/therapy-supplies/' },
    { fileName: 'acl-reconstruction-treatment-toronto', routeProd: '/common-injuries/acl-reconstruction-treatment-toronto/' },
    { fileName: 'hip-knee-osteoarthritis-treatment-toronto', routeProd: '/common-injuries/hip-knee-osteoarthritis-treatment-toronto/' },
    { fileName: 'knee-injuries-treatment-toronto', routeProd: '/common-injuries/knee-injuries-treatment-toronto/' },
    { fileName: 'about-us', routeProd: '/about-us/' },
    { fileName: 'our-team', routeProd: '/about-us/our-team/' },
    { fileName: 'blog', routeProd: '/blog/' },
    { fileName: 'contact-us', routeProd: '/contact-us/' },
    { fileName: 'physiotherapy-clinic-east-toronto', routeProd: '/contact-us/physiotherapy-clinic-east-toronto/' },
    { fileName: 'physiotherapy-clinic-trilogy-west-toronto', routeProd: '/contact-us/physiotherapy-clinic-trilogy-west-toronto/' },
    { fileName: 'locations', routeProd: '/locations/' },
    { fileName: 'landing', routeProd: '/landing' }
]

// https://www.uat.avalonphysiotherapy.ca
// https://www.avalonphysiotherapy.ca
export const routeAvalonphysiotherapy = [
    { fileName: 'home', routeProd: '/' },
    { fileName: 'services-physiotherapy', routeProd: '/subpage/Physiotherapy-massage-therapy-sports-wellness-strain-nl.html' },
    { fileName: 'services-massage-therapy', routeProd: '/subpage/Massage-Therapy-physio-avalon-rehab-neck-back-pain-nl.html' },
    { fileName: 'service-concussion-care', routeProd: '/subpage/Concussion-care.html' },
    { fileName: 'service-kinesiology', routeProd: '/subpage/Kinesiology.html' },
    { fileName: 'service-pelvic-health', routeProd: '/subpage/Pelvic-Health.html' },
    { fileName: 'locations', routeProd: '/page/locations-sports-hurt-neck-back-tissue-nl.html' },
    { fileName: 'staff', routeProd: '/page/Staff-strain-sprain-rehab-pain-therapy-newfoundland.html' },
    { fileName: 'contact', routeProd: '/page/contact-us-prescription-postural-injury-lower-back-avalon.html' },
    { fileName: 'education-tips', routeProd: '/page/Education-Tips.html' },
]

// https://rebalancetoronto.com
// https://uat.rebalancetoronto.com
export const routeRebalanceToronto = [
    { fileName: 'home', routeProd: '/' },
    { fileName: 'about-us', routeProd: '/about-us' },
    { fileName: 'our-philosophy', routeProd: '/about-us/our-philosophy/' },
    { fileName: 'careers', routeProd: '/about-us/careers/' },
    { fileName: 'our-team-physiotherapists', routeProd: '/about-us/our-team/#physiotherapists' },
    { fileName: 'our-team-sports-medicine', routeProd: '/about-us/our-team/#sportsmedicine' },
    { fileName: 'our-team-chiropractors', routeProd: '/about-us/our-team/#chiropractors' },
    { fileName: 'our-team-psychotherapists', routeProd: '/about-us/our-team/#psychotherapists' },
    { fileName: 'shockwave-therapy-downtown-toronto', routeProd: '/sports-medicine-services/shockwave-therapy-downtown-toronto/' },
    { fileName: 'sports-medicine-downtown-toronto', routeProd: '/sports-medicine-services/sports-medicine-downtown-toronto/' },
    { fileName: 'cosmetic-acupuncture-downtown-toronto', routeProd: '/sports-medicine-services/cosmetic-acupuncture-downtown-toronto/' },
    { fileName: 'pilates-downtown-toronto', routeProd: '/sports-medicine-services/pilates-downtown-toronto/' },
    { fileName: 'bracing-downtown-toronto', routeProd: '/sports-medicine-services/bracing-downtown-toronto/' },
    { fileName: 'general-wellness-products', routeProd: '/products/general-wellness-products/' },
    { fileName: 'faqs', routeProd: '/resources/faqs/' },
    { fileName: 'videos', routeProd: '/videos/' },
    { fileName: 'contact-us', routeProd: '/contact-us/' },
    { fileName: 'search-form', routeProd: '/search-form/' },
    { fileName: 'search-ontario', routeProd: '/?s=ontario' },
    { fileName: 'helpful-links', routeProd: '/resources/helpful-links/' },
];
