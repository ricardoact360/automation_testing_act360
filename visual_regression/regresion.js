import { chromium } from "playwright";
import { PNG } from "pngjs";
import fs from "fs";
import pixelmatch from "pixelmatch";
import path from "path";
import { fileURLToPath } from "url";
import dotenv from "dotenv";
dotenv.config();

const onlyShowDiffs = true;
const prodPage = 'https://mytorontophysio.com';
const devPage = 'https://uat.mytorontophysio.com';
import { routeMytorontophysio as routes } from './routes.js'; // Routes

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const viewports = [
  { name: 'desktop', width: 1280, height: 800 },
  /* { name: 'mobile', width: 430, height: 800 } */
];

// FIXME: Fix  Call log: - navigating to "", waiting until "load" at regresion.js:30:27 { name: 'TimeoutError' }
// Try ignoring that load and pass to next screenshot.
// TODO: Increase Time, more than 30s
(async () => {
    const browser = await chromium.launch();

    try {
        // Routes
        for (const { fileName, routeProd } of routes) {
            for (const { name, width, height } of viewports) {
                const context = await browser.newContext({
                    httpCredentials: { username: process.env.LIFEMARK_USERNAME, password: process.env.LIFEMARK_PASSWORD }, // UAT username and password here
                    viewport: { width, height }
                });

                const pageProd = await context.newPage();
                await pageProd.goto(prodPage + routeProd);
                await pageProd.waitForLoadState();

                const prodScreenshot = await pageProd.screenshot({ fullPage: true });

                const pageDev = await context.newPage();
                await pageDev.goto(devPage + routeProd);
                await pageDev.waitForLoadState();

                const devScreenshot = await pageDev.screenshot({ fullPage: true });

                const prodImage = PNG.sync.read(prodScreenshot);
                const devImage = PNG.sync.read(devScreenshot);

                const maxHeight = Math.max(prodImage.height, devImage.height);

                const resizeAndAlignImage = (image, height) => {
                    const resized = new PNG({ width: image.width, height });
                    PNG.bitblt(image, resized, 0, 0, image.width, image.height, 0, 0);
                    return resized;
                };

                const resizedProdImage = resizeAndAlignImage(prodImage, maxHeight);
                const resizedDevImage = resizeAndAlignImage(devImage, maxHeight);

                const diff = new PNG({
                    width: Math.max(resizedProdImage.width, resizedDevImage.width),
                    height: maxHeight,
                });

                const differences = pixelmatch(
                    resizedProdImage.data,
                    resizedDevImage.data,
                    diff.data,
                    diff.width,
                    diff.height,
                    { threshold: 0.1 }
                );

                console.log(`${fileName}-${name} compared. Found ${differences} differences`);

                const screenshotsDir = path.join(__dirname, "screenshots");
                if (!fs.existsSync(screenshotsDir)) {
                    fs.mkdirSync(screenshotsDir);
                }

                if (!onlyShowDiffs) {
                    fs.writeFileSync(path.join(screenshotsDir, `${fileName}-${name}-prod.png`), prodScreenshot);
                    fs.writeFileSync(path.join(screenshotsDir, `${fileName}-${name}-dev.png`), devScreenshot);
                }

                if (differences > 0) {
                    fs.writeFileSync(path.join(screenshotsDir, `${fileName}-${name}-diff.png`), PNG.sync.write(diff));
                    console.log(`✍️ Visual differences detected for ${fileName}-${name}`);
                } else {
                    console.log(`✅No visual differences detected for ${fileName}-${name}`);
                }

                await pageProd.close();
                await pageDev.close();
                await context.close();
            }
        }
    } catch (error) {
        console.error("Error occurred:", error);
    } finally {
        await browser.close();
    }
})();
