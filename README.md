# QA Automation

Made with [Playwright](https://playwright.dev).

## Installation
Git clone and go to the folder

```bash
cd automation_testing_act360
```
Install packages
```bash
npm install
```
*Copy and paste* the **env.example** and and rename it to **.env** and add missing information to that file.

## Usage
The most comfortable way to use it is with a graphical interface:

```bash
npx playwright test --ui
```
It will open a UI window like this:

![Playwright](./assets/images/playwrite_1.png)

You can also open the graphical interface in a specific test:
```bash
npx playwright test lifemark/lifeisnow --ui
```

## Caveats
- In the package.json: *playwright/test* **and** *playwright* should have **the same** version.
- In the gitlab-ci.yml: **docker-playwright** image should have the same version as the *playwright* package in the package.json.
- We should be aware of Playwright's updates, **this updated CAN BREAKE THE PIPELINE**

## About foms and email
We use **Mailnator** to receive emails, it is quite simple, no registration is required, just a **user number** which you will find in the *.env.example*. 

*Please note that emails are deleted approximately every 12 hours.*

Here you can see the mailbox: (https://www.mailinator.com/v4/public/inboxes.jsp?to= + *user number*)

![Mailnator](./assets/images/mailnator.png)

------------------------------------------------------------

#### With VsCode
Install Playwright Test for VSCode (https://marketplace.visualstudio.com/items?itemName=ms-playwright.playwright)

You will be able to run tests inside the IDE

![Playwright Test for VSCode](./assets/gifs/run_test.gif)

---------------------------------------------
## Uses of this program:
- Test forms and verify that the mail has arrived correctly..
- Visual regression testing desktop-mobiles.
- Test forms with ReCaptcha (*Work In Progress*)

---------------------------------------------
## Visual regression

### How to use:
1) Go to *visual_regression/regresion.js*
2) Change **prodPage** and **devPage** *variables*
![prod_uat_var](./assets/images/prod_uat_var.png)
3) Go to *visual_regression/routes.js* and **create or choose the routes you want to compare**
![object_route](./assets/images/object_route.png)
4) **Import** the object/routes to *visual_regression/regresion.js*
![route_export](./assets/images/routes_cap.png)
5) **Execute:**
```bash
node visual_regression/regresion.js 
```

- Look at visual_regression/screenshots folder
- By default I set onlyShowDiffs to false, so, will only give you the differences between Live/Production and UAT/Developmet.
You can clean screenshots folder with this command:

Delete screenshots:
```bash
cd ~/visual_regression/screenshots && rm -rf * && cd ..
```

---------------------------------------------
## Pipeline
If the pipeline fails you will receive an email

![email_pipeline_fail](./assets/images/pipeline_fail_mail.png)

You can click on the Pipeline number (#) and you will be redirected to the page where the pipeline is executed. You should click on the "download" icon (Artifacts) and open the **Playwright Report**.

![img_pipelinepage](./assets/images/pipeline_page.png)

## Or you can access from the project:
![git_pipelinepage](./assets/gifs/qa_pipeline_artifacts.gif)

## About Playwright Report
Playwright Test will categorize tests as follows:

- **"passed"** - tests that passed on the first run;
- **"flaky"** - tests that failed on the first run, but passed when retried;
- **"failed"** - tests that failed on the first run and failed all retries.

### Be aware:
- Check the **env variables of the pipeline** here: https://gitlab.com/ricardoact360/automation_testing_act360/-/settings/ci_cd
- The pipeline can take many minutes to run.
---------------------------------------------
## About Recaptcha / Antibot
This is the list of websites with test problems due to Recaptcha or an antibot script. Any test that includes submit will be **skipped**, for now.

- Karprehab:
    - Contact Us (https://karprehab.com/vancouver-rehabilitation-contact-information/)
    - Referrals (https://karprehab.com/referrals/)
- FitForWork 
  - https://fitforwork.com/services/referral-form 
  - https://fitforwork.com/contact/contact-us

---------------------------------------------
## Troubleshooting
- Send an email to Ricardo@act360.ca

#### Common issues:
- If the behavior of the website has changed due to customer requirements, you will need to change the test to accommodate.
- Sometimes tests may fail because the page is slow loading content. You should proceed to run the test again or increase the waiting time in the test.
- **Some websites** requires a `await page.goto(url, { waitUntil: "domcontentloaded" });` *in the code* in order to load/work properly.
- A simple test can fail via pipeline for multiple reasons. Take into consideration the page load time or that the pipeline runs on a server with very simple capabilities, unlike the computer you own that may have multiple cores.
