import axios from 'axios';
import dotenv from "dotenv";

dotenv.config();

const API_KEY = process.env.CAPSOLVER_API_KEY;

// NOTE: recaptchaType
// 'ReCaptchaV2TaskProxyLess',
// 'ReCaptchaV3TaskProxyLess'
// LOOK IF ITS INVISIBLE (With The Chrome Extension) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// https://docs.capsolver.com/guide/api-createtask.html
// MIRAR PAGE ACTION SE ENCUENTRA EN DOCUMENTACION + Chrome Extention
export async function capsolver(taskConfig) {
  const payload = {
    clientKey: API_KEY,
    task: {
      type: taskConfig.type,
      websiteKey: taskConfig.websiteKey,
      websiteURL: taskConfig.websiteURL,
      isInvisible: taskConfig.isInvisible !== undefined ? taskConfig.isInvisible : false,
      pageAction: taskConfig.pageAction || 'recaptcha_v3_action'
    }
  };

  try {
    const res = await axios.post("https://api.capsolver.com/createTask", payload);
    const task_id = res.data.taskId;
    if (!task_id) {
      console.log("Failed to create task:", res.data);
      return;
    }
    console.log("Got taskId:", task_id);

    while (true) {
      await new Promise(resolve => setTimeout(resolve, 1000)); // Delay for 1 second

      const getResultPayload = {clientKey: API_KEY, taskId: task_id};
      const resp = await axios.post("https://api.capsolver.com/getTaskResult", getResultPayload);
      const status = resp.data.status;

      if (status === "ready") {
        return resp.data.solution.gRecaptchaResponse;
      }
      if (status === "failed" || resp.data.errorId) {
        console.log("Solve failed! response:", resp.data);
        return;
      }
    }
  } catch (error) {
    console.error("Error:", error);
  }
}

