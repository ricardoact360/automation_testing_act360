export function getRandomFloat(min, max) {
  return (Math.random() * (max - min) + min).toFixed(2);
}

