export const randomString = () => {
    const allChar = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    
    for (let i = 0; i < 5; i++) {
      const indice = Math.floor(Math.random() * allChar.length);
      result += allChar.charAt(indice);
    }
    
    return result;
};